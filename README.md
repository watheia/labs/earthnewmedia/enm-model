# earthnew.media Site Model

`enm-model` is a collection of projects which use Eclipse Modeling Framework (EMF) to develop a domain driven entity model for the site. EMF can be used to generate source models, data exchange formats, and even fully functional form based UI's. More info can be found here <https://eclipsesource.com/blogs/tutorials/emf-tutorial/>

![Site Model Diagram](earthnewmedia.model/model/model.jpg)