/**
 */
package earthnewmedia.model;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Content
 * Metadata</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link earthnewmedia.model.ContentMetadata#getUri <em>Uri</em>}</li>
 * <li>{@link earthnewmedia.model.ContentMetadata#getMimetype
 * <em>Mimetype</em>}</li>
 * </ul>
 *
 * @see earthnewmedia.model.ModelPackage#getContentMetadata()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface ContentMetadata extends EObject {
    /**
     * Returns the value of the '<em><b>Uri</b></em>' attribute. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Uri</em>' attribute.
     * @see #setUri(String)
     * @see earthnewmedia.model.ModelPackage#getContentMetadata_Uri()
     * @model required="true"
     * @generated
     */
    String getUri();

    /**
     * Sets the value of the '{@link earthnewmedia.model.ContentMetadata#getUri
     * <em>Uri</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Uri</em>' attribute.
     * @see #getUri()
     * @generated
     */
    void setUri(String value);

    /**
     * Returns the value of the '<em><b>Mimetype</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Mimetype</em>' attribute.
     * @see #setMimetype(String)
     * @see earthnewmedia.model.ModelPackage#getContentMetadata_Mimetype()
     * @model required="true"
     * @generated
     */
    String getMimetype();

    /**
     * Sets the value of the '{@link earthnewmedia.model.ContentMetadata#getMimetype
     * <em>Mimetype</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Mimetype</em>' attribute.
     * @see #getMimetype()
     * @generated
     */
    void setMimetype(String value);

} // ContentMetadata
