/**
 */
package earthnewmedia.model;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Audio
 * Metadata</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link earthnewmedia.model.AudioMetadata#getDuration
 * <em>Duration</em>}</li>
 * </ul>
 *
 * @see earthnewmedia.model.ModelPackage#getAudioMetadata()
 * @model
 * @generated
 */
public interface AudioMetadata extends ContentMetadata {
    /**
     * Returns the value of the '<em><b>Duration</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Duration</em>' attribute.
     * @see #setDuration(int)
     * @see earthnewmedia.model.ModelPackage#getAudioMetadata_Duration()
     * @model
     * @generated
     */
    int getDuration();

    /**
     * Sets the value of the '{@link earthnewmedia.model.AudioMetadata#getDuration
     * <em>Duration</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Duration</em>' attribute.
     * @see #getDuration()
     * @generated
     */
    void setDuration(int value);

} // AudioMetadata
