/**
 */
package earthnewmedia.model;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc --> A representation of the literals of the enumeration
 * '<em><b>Library Type</b></em>', and utility methods for working with them.
 * <!-- end-user-doc -->
 * 
 * @see earthnewmedia.model.ModelPackage#getLibraryType()
 * @model
 * @generated
 */
public enum LibraryType implements Enumerator {
    /**
     * The '<em><b>DISCUSSION</b></em>' literal object. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @see #DISCUSSION_VALUE
     * @generated
     * @ordered
     */
    DISCUSSION(0, "DISCUSSION", "DISCUSSION"),

    /**
     * The '<em><b>ALBUM</b></em>' literal object. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @see #ALBUM_VALUE
     * @generated
     * @ordered
     */
    ALBUM(1, "ALBUM", "ALBUM"),

    /**
     * The '<em><b>GALLERY</b></em>' literal object. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @see #GALLERY_VALUE
     * @generated
     * @ordered
     */
    GALLERY(2, "GALLERY", "GALLERY");

    /**
     * The '<em><b>DISCUSSION</b></em>' literal value. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @see #DISCUSSION
     * @model
     * @generated
     * @ordered
     */
    public static final int DISCUSSION_VALUE = 0;

    /**
     * The '<em><b>ALBUM</b></em>' literal value. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @see #ALBUM
     * @model
     * @generated
     * @ordered
     */
    public static final int ALBUM_VALUE = 1;

    /**
     * The '<em><b>GALLERY</b></em>' literal value. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @see #GALLERY
     * @model
     * @generated
     * @ordered
     */
    public static final int GALLERY_VALUE = 2;

    /**
     * An array of all the '<em><b>Library Type</b></em>' enumerators. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private static final LibraryType[] VALUES_ARRAY = new LibraryType[] {
            DISCUSSION,
            ALBUM,
            GALLERY,
    };

    /**
     * A public read-only list of all the '<em><b>Library Type</b></em>'
     * enumerators. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public static final List<LibraryType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

    /**
     * Returns the '<em><b>Library Type</b></em>' literal with the specified literal
     * value. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param literal the literal.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static LibraryType get(String literal) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            LibraryType result = VALUES_ARRAY[i];
            if (result.toString().equals(literal)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>Library Type</b></em>' literal with the specified name.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param name the name.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static LibraryType getByName(String name) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            LibraryType result = VALUES_ARRAY[i];
            if (result.getName().equals(name)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>Library Type</b></em>' literal with the specified integer
     * value. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param value the integer value.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static LibraryType get(int value) {
        switch (value) {
        case DISCUSSION_VALUE:
            return DISCUSSION;
        case ALBUM_VALUE:
            return ALBUM;
        case GALLERY_VALUE:
            return GALLERY;
        }
        return null;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private final int value;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private final String name;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private final String literal;

    /**
     * Only this class can construct instances. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     */
    private LibraryType(int value, String name, String literal) {
        this.value = value;
        this.name = name;
        this.literal = literal;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public int getValue() {
        return value;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getLiteral() {
        return literal;
    }

    /**
     * Returns the literal value of the enumerator, which is its string
     * representation. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        return literal;
    }

} // LibraryType
