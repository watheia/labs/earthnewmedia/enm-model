/**
 */
package earthnewmedia.model;

import java.util.Date;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Library
 * Item</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link earthnewmedia.model.LibraryItem#getTitle <em>Title</em>}</li>
 * <li>{@link earthnewmedia.model.LibraryItem#getDescription
 * <em>Description</em>}</li>
 * <li>{@link earthnewmedia.model.LibraryItem#getComments
 * <em>Comments</em>}</li>
 * <li>{@link earthnewmedia.model.LibraryItem#getMetadata
 * <em>Metadata</em>}</li>
 * <li>{@link earthnewmedia.model.LibraryItem#getCreatedDate <em>Created
 * Date</em>}</li>
 * <li>{@link earthnewmedia.model.LibraryItem#getSlug <em>Slug</em>}</li>
 * </ul>
 *
 * @see earthnewmedia.model.ModelPackage#getLibraryItem()
 * @model
 * @generated
 */
public interface LibraryItem extends Entity {
    /**
     * Returns the value of the '<em><b>Title</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Title</em>' attribute.
     * @see #setTitle(String)
     * @see earthnewmedia.model.ModelPackage#getLibraryItem_Title()
     * @model required="true"
     * @generated
     */
    String getTitle();

    /**
     * Sets the value of the '{@link earthnewmedia.model.LibraryItem#getTitle
     * <em>Title</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Title</em>' attribute.
     * @see #getTitle()
     * @generated
     */
    void setTitle(String value);

    /**
     * Returns the value of the '<em><b>Description</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Description</em>' attribute.
     * @see #setDescription(String)
     * @see earthnewmedia.model.ModelPackage#getLibraryItem_Description()
     * @model
     * @generated
     */
    String getDescription();

    /**
     * Sets the value of the '{@link earthnewmedia.model.LibraryItem#getDescription
     * <em>Description</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
     * -->
     * 
     * @param value the new value of the '<em>Description</em>' attribute.
     * @see #getDescription()
     * @generated
     */
    void setDescription(String value);

    /**
     * Returns the value of the '<em><b>Comments</b></em>' containment reference
     * list. The list contents are of type {@link earthnewmedia.model.Comment}. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Comments</em>' containment reference list.
     * @see earthnewmedia.model.ModelPackage#getLibraryItem_Comments()
     * @model containment="true"
     * @generated
     */
    EList<Comment> getComments();

    /**
     * Returns the value of the '<em><b>Metadata</b></em>' containment reference.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Metadata</em>' containment reference.
     * @see #setMetadata(ContentMetadata)
     * @see earthnewmedia.model.ModelPackage#getLibraryItem_Metadata()
     * @model containment="true" required="true"
     * @generated
     */
    ContentMetadata getMetadata();

    /**
     * Sets the value of the '{@link earthnewmedia.model.LibraryItem#getMetadata
     * <em>Metadata</em>}' containment reference. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @param value the new value of the '<em>Metadata</em>' containment reference.
     * @see #getMetadata()
     * @generated
     */
    void setMetadata(ContentMetadata value);

    /**
     * Returns the value of the '<em><b>Created Date</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Created Date</em>' attribute.
     * @see #setCreatedDate(Date)
     * @see earthnewmedia.model.ModelPackage#getLibraryItem_CreatedDate()
     * @model
     * @generated
     */
    Date getCreatedDate();

    /**
     * Sets the value of the '{@link earthnewmedia.model.LibraryItem#getCreatedDate
     * <em>Created Date</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
     * -->
     * 
     * @param value the new value of the '<em>Created Date</em>' attribute.
     * @see #getCreatedDate()
     * @generated
     */
    void setCreatedDate(Date value);

    /**
     * Returns the value of the '<em><b>Slug</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Slug</em>' attribute.
     * @see #setSlug(String)
     * @see earthnewmedia.model.ModelPackage#getLibraryItem_Slug()
     * @model required="true"
     * @generated
     */
    String getSlug();

    /**
     * Sets the value of the '{@link earthnewmedia.model.LibraryItem#getSlug
     * <em>Slug</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Slug</em>' attribute.
     * @see #getSlug()
     * @generated
     */
    void setSlug(String value);

} // LibraryItem
