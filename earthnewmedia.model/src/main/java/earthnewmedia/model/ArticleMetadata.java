/**
 */
package earthnewmedia.model;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Article
 * Metadata</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link earthnewmedia.model.ArticleMetadata#getImage <em>Image</em>}</li>
 * </ul>
 *
 * @see earthnewmedia.model.ModelPackage#getArticleMetadata()
 * @model
 * @generated
 */
public interface ArticleMetadata extends ContentMetadata {
    /**
     * Returns the value of the '<em><b>Image</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Image</em>' attribute.
     * @see #setImage(String)
     * @see earthnewmedia.model.ModelPackage#getArticleMetadata_Image()
     * @model
     * @generated
     */
    String getImage();

    /**
     * Sets the value of the '{@link earthnewmedia.model.ArticleMetadata#getImage
     * <em>Image</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Image</em>' attribute.
     * @see #getImage()
     * @generated
     */
    void setImage(String value);

} // ArticleMetadata
