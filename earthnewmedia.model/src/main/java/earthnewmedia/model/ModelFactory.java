/**
 */
package earthnewmedia.model;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc --> The <b>Factory</b> for the model. It provides a
 * create method for each non-abstract class of the model. <!-- end-user-doc -->
 * 
 * @see earthnewmedia.model.ModelPackage
 * @generated
 */
public interface ModelFactory extends EFactory {
    /**
     * The singleton instance of the factory. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     */
    ModelFactory eINSTANCE = earthnewmedia.model.impl.ModelFactoryImpl.init();

    /**
     * Returns a new object of class '<em>User</em>'. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @return a new object of class '<em>User</em>'.
     * @generated
     */
    User createUser();

    /**
     * Returns a new object of class '<em>Credentials</em>'. <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Credentials</em>'.
     * @generated
     */
    Credentials createCredentials();

    /**
     * Returns a new object of class '<em>Member Attribute</em>'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Member Attribute</em>'.
     * @generated
     */
    MemberAttribute createMemberAttribute();

    /**
     * Returns a new object of class '<em>Library</em>'. <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Library</em>'.
     * @generated
     */
    Library createLibrary();

    /**
     * Returns a new object of class '<em>Library Item</em>'. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Library Item</em>'.
     * @generated
     */
    LibraryItem createLibraryItem();

    /**
     * Returns a new object of class '<em>Comment</em>'. <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Comment</em>'.
     * @generated
     */
    Comment createComment();

    /**
     * Returns a new object of class '<em>Article Metadata</em>'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Article Metadata</em>'.
     * @generated
     */
    ArticleMetadata createArticleMetadata();

    /**
     * Returns a new object of class '<em>Image Metadata</em>'. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Image Metadata</em>'.
     * @generated
     */
    ImageMetadata createImageMetadata();

    /**
     * Returns a new object of class '<em>Audio Metadata</em>'. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Audio Metadata</em>'.
     * @generated
     */
    AudioMetadata createAudioMetadata();

    /**
     * Returns a new object of class '<em>Video Metadata</em>'. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @return a new object of class '<em>Video Metadata</em>'.
     * @generated
     */
    VideoMetadata createVideoMetadata();

    /**
     * Returns the package supported by this factory. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @return the package supported by this factory.
     * @generated
     */
    ModelPackage getModelPackage();

} // ModelFactory
