/**
 */
package earthnewmedia.model;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc --> A representation of the literals of the enumeration
 * '<em><b>User Role</b></em>', and utility methods for working with them. <!--
 * end-user-doc -->
 * 
 * @see earthnewmedia.model.ModelPackage#getUserRole()
 * @model
 * @generated
 */
public enum UserRole implements Enumerator {
    /**
     * The '<em><b>ADMIN</b></em>' literal object. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @see #ADMIN_VALUE
     * @generated
     * @ordered
     */
    ADMIN(0, "ADMIN", "ADMIN"),

    /**
     * The '<em><b>SUPPORT</b></em>' literal object. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @see #SUPPORT_VALUE
     * @generated
     * @ordered
     */
    SUPPORT(1, "SUPPORT", "SUPPORT"),

    /**
     * The '<em><b>MEMBER</b></em>' literal object. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @see #MEMBER_VALUE
     * @generated
     * @ordered
     */
    MEMBER(2, "MEMBER", "MEMBER");

    /**
     * The '<em><b>ADMIN</b></em>' literal value. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @see #ADMIN
     * @model
     * @generated
     * @ordered
     */
    public static final int ADMIN_VALUE = 0;

    /**
     * The '<em><b>SUPPORT</b></em>' literal value. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @see #SUPPORT
     * @model
     * @generated
     * @ordered
     */
    public static final int SUPPORT_VALUE = 1;

    /**
     * The '<em><b>MEMBER</b></em>' literal value. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @see #MEMBER
     * @model
     * @generated
     * @ordered
     */
    public static final int MEMBER_VALUE = 2;

    /**
     * An array of all the '<em><b>User Role</b></em>' enumerators. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private static final UserRole[] VALUES_ARRAY = new UserRole[] {
            ADMIN,
            SUPPORT,
            MEMBER,
    };

    /**
     * A public read-only list of all the '<em><b>User Role</b></em>' enumerators.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public static final List<UserRole> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

    /**
     * Returns the '<em><b>User Role</b></em>' literal with the specified literal
     * value. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param literal the literal.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static UserRole get(String literal) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            UserRole result = VALUES_ARRAY[i];
            if (result.toString().equals(literal)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>User Role</b></em>' literal with the specified name. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param name the name.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static UserRole getByName(String name) {
        for (int i = 0; i < VALUES_ARRAY.length; ++i) {
            UserRole result = VALUES_ARRAY[i];
            if (result.getName().equals(name)) {
                return result;
            }
        }
        return null;
    }

    /**
     * Returns the '<em><b>User Role</b></em>' literal with the specified integer
     * value. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param value the integer value.
     * @return the matching enumerator or <code>null</code>.
     * @generated
     */
    public static UserRole get(int value) {
        switch (value) {
        case ADMIN_VALUE:
            return ADMIN;
        case SUPPORT_VALUE:
            return SUPPORT;
        case MEMBER_VALUE:
            return MEMBER;
        }
        return null;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private final int value;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private final String name;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private final String literal;

    /**
     * Only this class can construct instances. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     */
    private UserRole(int value, String name, String literal) {
        this.value = value;
        this.name = name;
        this.literal = literal;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public int getValue() {
        return value;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getLiteral() {
        return literal;
    }

    /**
     * Returns the literal value of the enumerator, which is its string
     * representation. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        return literal;
    }

} // UserRole
