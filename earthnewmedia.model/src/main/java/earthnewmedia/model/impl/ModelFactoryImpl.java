/**
 */
package earthnewmedia.model.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import earthnewmedia.model.AccessLevel;
import earthnewmedia.model.ArticleMetadata;
import earthnewmedia.model.AudioMetadata;
import earthnewmedia.model.Comment;
import earthnewmedia.model.Credentials;
import earthnewmedia.model.HashAlgo;
import earthnewmedia.model.ImageMetadata;
import earthnewmedia.model.Library;
import earthnewmedia.model.LibraryItem;
import earthnewmedia.model.LibraryType;
import earthnewmedia.model.MemberAttribute;
import earthnewmedia.model.ModelFactory;
import earthnewmedia.model.ModelPackage;
import earthnewmedia.model.User;
import earthnewmedia.model.UserRole;
import earthnewmedia.model.VideoMetadata;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Factory</b>. <!--
 * end-user-doc -->
 * 
 * @generated
 */
public class ModelFactoryImpl extends EFactoryImpl implements ModelFactory {
    /**
     * Creates the default factory implementation. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     */
    public static ModelFactory init() {
        try {
            ModelFactory theModelFactory = (ModelFactory) EPackage.Registry.INSTANCE.getEFactory(ModelPackage.eNS_URI);
            if (theModelFactory != null) {
                return theModelFactory;
            }
        } catch (Exception exception) {
            EcorePlugin.INSTANCE.log(exception);
        }
        return new ModelFactoryImpl();
    }

    /**
     * Creates an instance of the factory. <!-- begin-user-doc --> <!-- end-user-doc
     * -->
     * 
     * @generated
     */
    public ModelFactoryImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EObject create(EClass eClass) {
        switch (eClass.getClassifierID()) {
        case ModelPackage.USER:
            return createUser();
        case ModelPackage.CREDENTIALS:
            return createCredentials();
        case ModelPackage.MEMBER_ATTRIBUTE:
            return createMemberAttribute();
        case ModelPackage.LIBRARY:
            return createLibrary();
        case ModelPackage.LIBRARY_ITEM:
            return createLibraryItem();
        case ModelPackage.COMMENT:
            return createComment();
        case ModelPackage.ARTICLE_METADATA:
            return createArticleMetadata();
        case ModelPackage.IMAGE_METADATA:
            return createImageMetadata();
        case ModelPackage.AUDIO_METADATA:
            return createAudioMetadata();
        case ModelPackage.VIDEO_METADATA:
            return createVideoMetadata();
        default:
            throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
        }
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object createFromString(EDataType eDataType, String initialValue) {
        switch (eDataType.getClassifierID()) {
        case ModelPackage.ACCESS_LEVEL:
            return createAccessLevelFromString(eDataType, initialValue);
        case ModelPackage.USER_ROLE:
            return createUserRoleFromString(eDataType, initialValue);
        case ModelPackage.HASH_ALGO:
            return createHashAlgoFromString(eDataType, initialValue);
        case ModelPackage.LIBRARY_TYPE:
            return createLibraryTypeFromString(eDataType, initialValue);
        default:
            throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
        }
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String convertToString(EDataType eDataType, Object instanceValue) {
        switch (eDataType.getClassifierID()) {
        case ModelPackage.ACCESS_LEVEL:
            return convertAccessLevelToString(eDataType, instanceValue);
        case ModelPackage.USER_ROLE:
            return convertUserRoleToString(eDataType, instanceValue);
        case ModelPackage.HASH_ALGO:
            return convertHashAlgoToString(eDataType, instanceValue);
        case ModelPackage.LIBRARY_TYPE:
            return convertLibraryTypeToString(eDataType, instanceValue);
        default:
            throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
        }
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public User createUser() {
        UserImpl user = new UserImpl();
        return user;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Credentials createCredentials() {
        CredentialsImpl credentials = new CredentialsImpl();
        return credentials;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public MemberAttribute createMemberAttribute() {
        MemberAttributeImpl memberAttribute = new MemberAttributeImpl();
        return memberAttribute;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Library createLibrary() {
        LibraryImpl library = new LibraryImpl();
        return library;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public LibraryItem createLibraryItem() {
        LibraryItemImpl libraryItem = new LibraryItemImpl();
        return libraryItem;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Comment createComment() {
        CommentImpl comment = new CommentImpl();
        return comment;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public ArticleMetadata createArticleMetadata() {
        ArticleMetadataImpl articleMetadata = new ArticleMetadataImpl();
        return articleMetadata;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public ImageMetadata createImageMetadata() {
        ImageMetadataImpl imageMetadata = new ImageMetadataImpl();
        return imageMetadata;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public AudioMetadata createAudioMetadata() {
        AudioMetadataImpl audioMetadata = new AudioMetadataImpl();
        return audioMetadata;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public VideoMetadata createVideoMetadata() {
        VideoMetadataImpl videoMetadata = new VideoMetadataImpl();
        return videoMetadata;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public AccessLevel createAccessLevelFromString(EDataType eDataType, String initialValue) {
        AccessLevel result = AccessLevel.get(initialValue);
        if (result == null)
            throw new IllegalArgumentException(
                    "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
        return result;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public String convertAccessLevelToString(EDataType eDataType, Object instanceValue) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public UserRole createUserRoleFromString(EDataType eDataType, String initialValue) {
        UserRole result = UserRole.get(initialValue);
        if (result == null)
            throw new IllegalArgumentException(
                    "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
        return result;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public String convertUserRoleToString(EDataType eDataType, Object instanceValue) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public HashAlgo createHashAlgoFromString(EDataType eDataType, String initialValue) {
        HashAlgo result = HashAlgo.get(initialValue);
        if (result == null)
            throw new IllegalArgumentException(
                    "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
        return result;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public String convertHashAlgoToString(EDataType eDataType, Object instanceValue) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public LibraryType createLibraryTypeFromString(EDataType eDataType, String initialValue) {
        LibraryType result = LibraryType.get(initialValue);
        if (result == null)
            throw new IllegalArgumentException(
                    "The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
        return result;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public String convertLibraryTypeToString(EDataType eDataType, Object instanceValue) {
        return instanceValue == null ? null : instanceValue.toString();
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public ModelPackage getModelPackage() {
        return (ModelPackage) getEPackage();
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @deprecated
     * @generated
     */
    @Deprecated
    public static ModelPackage getPackage() {
        return ModelPackage.eINSTANCE;
    }

} // ModelFactoryImpl
