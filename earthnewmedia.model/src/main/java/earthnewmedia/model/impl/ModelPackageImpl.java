/**
 */
package earthnewmedia.model.impl;

import earthnewmedia.model.AccessLevel;
import earthnewmedia.model.ArticleMetadata;
import earthnewmedia.model.AudioMetadata;
import earthnewmedia.model.Comment;
import earthnewmedia.model.ContentMetadata;
import earthnewmedia.model.Credentials;
import earthnewmedia.model.Entity;
import earthnewmedia.model.HashAlgo;
import earthnewmedia.model.ImageMetadata;
import earthnewmedia.model.Library;
import earthnewmedia.model.LibraryItem;
import earthnewmedia.model.LibraryType;
import earthnewmedia.model.MemberAttribute;
import earthnewmedia.model.ModelFactory;
import earthnewmedia.model.ModelPackage;
import earthnewmedia.model.User;
import earthnewmedia.model.UserRole;
import earthnewmedia.model.VideoMetadata;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Package</b>. <!--
 * end-user-doc -->
 * 
 * @generated
 */
public class ModelPackageImpl extends EPackageImpl implements ModelPackage {
    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass entityEClass = null;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass userEClass = null;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass credentialsEClass = null;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass memberAttributeEClass = null;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass libraryEClass = null;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass libraryItemEClass = null;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass commentEClass = null;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass contentMetadataEClass = null;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass articleMetadataEClass = null;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass imageMetadataEClass = null;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass audioMetadataEClass = null;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private EClass videoMetadataEClass = null;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum accessLevelEEnum = null;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum userRoleEEnum = null;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum hashAlgoEEnum = null;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private EEnum libraryTypeEEnum = null;

    /**
     * Creates an instance of the model <b>Package</b>, registered with
     * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the
     * package package URI value.
     * <p>
     * Note: the correct way to create the package is via the static factory method
     * {@link #init init()}, which also performs initialization of the package, or
     * returns the registered package, if one already exists. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @see org.eclipse.emf.ecore.EPackage.Registry
     * @see earthnewmedia.model.ModelPackage#eNS_URI
     * @see #init()
     * @generated
     */
    private ModelPackageImpl() {
        super(eNS_URI, ModelFactory.eINSTANCE);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private static boolean isInited = false;

    /**
     * Creates, registers, and initializes the <b>Package</b> for this model, and
     * for any others upon which it depends.
     *
     * <p>
     * This method is used to initialize {@link ModelPackage#eINSTANCE} when that
     * field is accessed. Clients should not invoke it directly. Instead, they
     * should simply access that field to obtain the package. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @see #eNS_URI
     * @see #createPackageContents()
     * @see #initializePackageContents()
     * @generated
     */
    public static ModelPackage init() {
        if (isInited)
            return (ModelPackage) EPackage.Registry.INSTANCE.getEPackage(ModelPackage.eNS_URI);

        // Obtain or create and register package
        Object registeredModelPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
        ModelPackageImpl theModelPackage = registeredModelPackage instanceof ModelPackageImpl
                ? (ModelPackageImpl) registeredModelPackage
                : new ModelPackageImpl();

        isInited = true;

        // Create package meta-data objects
        theModelPackage.createPackageContents();

        // Initialize created meta-data
        theModelPackage.initializePackageContents();

        // Mark meta-data to indicate it can't be changed
        theModelPackage.freeze();

        // Update the registry and return the package
        EPackage.Registry.INSTANCE.put(ModelPackage.eNS_URI, theModelPackage);
        return theModelPackage;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getEntity() {
        return entityEClass;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getEntity_Id() {
        return (EAttribute) entityEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getUser() {
        return userEClass;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getUser_UserName() {
        return (EAttribute) userEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getUser_Credentials() {
        return (EReference) userEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getUser_Email() {
        return (EAttribute) userEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getUser_Attributes() {
        return (EReference) userEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getUser_Libraries() {
        return (EReference) userEClass.getEStructuralFeatures().get(4);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getCredentials() {
        return credentialsEClass;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getCredentials_Algo() {
        return (EAttribute) credentialsEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getCredentials_Value() {
        return (EAttribute) credentialsEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getMemberAttribute() {
        return memberAttributeEClass;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getMemberAttribute_Label() {
        return (EAttribute) memberAttributeEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getMemberAttribute_Content() {
        return (EAttribute) memberAttributeEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getLibrary() {
        return libraryEClass;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getLibrary_Slug() {
        return (EAttribute) libraryEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getLibrary_Name() {
        return (EAttribute) libraryEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getLibrary_Description() {
        return (EAttribute) libraryEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getLibrary_Type() {
        return (EAttribute) libraryEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getLibrary_CreatedDate() {
        return (EAttribute) libraryEClass.getEStructuralFeatures().get(4);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getLibrary_Access() {
        return (EAttribute) libraryEClass.getEStructuralFeatures().get(5);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getLibrary_Items() {
        return (EReference) libraryEClass.getEStructuralFeatures().get(6);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getLibraryItem() {
        return libraryItemEClass;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getLibraryItem_Title() {
        return (EAttribute) libraryItemEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getLibraryItem_Description() {
        return (EAttribute) libraryItemEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getLibraryItem_Comments() {
        return (EReference) libraryItemEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getLibraryItem_Metadata() {
        return (EReference) libraryItemEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getLibraryItem_CreatedDate() {
        return (EAttribute) libraryItemEClass.getEStructuralFeatures().get(4);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getLibraryItem_Slug() {
        return (EAttribute) libraryItemEClass.getEStructuralFeatures().get(5);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getComment() {
        return commentEClass;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getComment_Replies() {
        return (EReference) commentEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EReference getComment_Owner() {
        return (EReference) commentEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getComment_Content() {
        return (EAttribute) commentEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getComment_CreatedDate() {
        return (EAttribute) commentEClass.getEStructuralFeatures().get(3);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getContentMetadata() {
        return contentMetadataEClass;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getContentMetadata_Uri() {
        return (EAttribute) contentMetadataEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getContentMetadata_Mimetype() {
        return (EAttribute) contentMetadataEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getArticleMetadata() {
        return articleMetadataEClass;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getArticleMetadata_Image() {
        return (EAttribute) articleMetadataEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getImageMetadata() {
        return imageMetadataEClass;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getImageMetadata_Width() {
        return (EAttribute) imageMetadataEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getImageMetadata_Height() {
        return (EAttribute) imageMetadataEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getAudioMetadata() {
        return audioMetadataEClass;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getAudioMetadata_Duration() {
        return (EAttribute) audioMetadataEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EClass getVideoMetadata() {
        return videoMetadataEClass;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getVideoMetadata_Duration() {
        return (EAttribute) videoMetadataEClass.getEStructuralFeatures().get(0);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getVideoMetadata_Width() {
        return (EAttribute) videoMetadataEClass.getEStructuralFeatures().get(1);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EAttribute getVideoMetadata_Height() {
        return (EAttribute) videoMetadataEClass.getEStructuralFeatures().get(2);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getAccessLevel() {
        return accessLevelEEnum;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getUserRole() {
        return userRoleEEnum;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getHashAlgo() {
        return hashAlgoEEnum;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EEnum getLibraryType() {
        return libraryTypeEEnum;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public ModelFactory getModelFactory() {
        return (ModelFactory) getEFactoryInstance();
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private boolean isCreated = false;

    /**
     * Creates the meta-model objects for the package. This method is guarded to
     * have no affect on any invocation but its first. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     */
    public void createPackageContents() {
        if (isCreated)
            return;
        isCreated = true;

        // Create classes and their features
        entityEClass = createEClass(ENTITY);
        createEAttribute(entityEClass, ENTITY__ID);

        userEClass = createEClass(USER);
        createEAttribute(userEClass, USER__USER_NAME);
        createEReference(userEClass, USER__CREDENTIALS);
        createEAttribute(userEClass, USER__EMAIL);
        createEReference(userEClass, USER__ATTRIBUTES);
        createEReference(userEClass, USER__LIBRARIES);

        credentialsEClass = createEClass(CREDENTIALS);
        createEAttribute(credentialsEClass, CREDENTIALS__ALGO);
        createEAttribute(credentialsEClass, CREDENTIALS__VALUE);

        memberAttributeEClass = createEClass(MEMBER_ATTRIBUTE);
        createEAttribute(memberAttributeEClass, MEMBER_ATTRIBUTE__LABEL);
        createEAttribute(memberAttributeEClass, MEMBER_ATTRIBUTE__CONTENT);

        libraryEClass = createEClass(LIBRARY);
        createEAttribute(libraryEClass, LIBRARY__SLUG);
        createEAttribute(libraryEClass, LIBRARY__NAME);
        createEAttribute(libraryEClass, LIBRARY__DESCRIPTION);
        createEAttribute(libraryEClass, LIBRARY__TYPE);
        createEAttribute(libraryEClass, LIBRARY__CREATED_DATE);
        createEAttribute(libraryEClass, LIBRARY__ACCESS);
        createEReference(libraryEClass, LIBRARY__ITEMS);

        libraryItemEClass = createEClass(LIBRARY_ITEM);
        createEAttribute(libraryItemEClass, LIBRARY_ITEM__TITLE);
        createEAttribute(libraryItemEClass, LIBRARY_ITEM__DESCRIPTION);
        createEReference(libraryItemEClass, LIBRARY_ITEM__COMMENTS);
        createEReference(libraryItemEClass, LIBRARY_ITEM__METADATA);
        createEAttribute(libraryItemEClass, LIBRARY_ITEM__CREATED_DATE);
        createEAttribute(libraryItemEClass, LIBRARY_ITEM__SLUG);

        commentEClass = createEClass(COMMENT);
        createEReference(commentEClass, COMMENT__REPLIES);
        createEReference(commentEClass, COMMENT__OWNER);
        createEAttribute(commentEClass, COMMENT__CONTENT);
        createEAttribute(commentEClass, COMMENT__CREATED_DATE);

        contentMetadataEClass = createEClass(CONTENT_METADATA);
        createEAttribute(contentMetadataEClass, CONTENT_METADATA__URI);
        createEAttribute(contentMetadataEClass, CONTENT_METADATA__MIMETYPE);

        articleMetadataEClass = createEClass(ARTICLE_METADATA);
        createEAttribute(articleMetadataEClass, ARTICLE_METADATA__IMAGE);

        imageMetadataEClass = createEClass(IMAGE_METADATA);
        createEAttribute(imageMetadataEClass, IMAGE_METADATA__WIDTH);
        createEAttribute(imageMetadataEClass, IMAGE_METADATA__HEIGHT);

        audioMetadataEClass = createEClass(AUDIO_METADATA);
        createEAttribute(audioMetadataEClass, AUDIO_METADATA__DURATION);

        videoMetadataEClass = createEClass(VIDEO_METADATA);
        createEAttribute(videoMetadataEClass, VIDEO_METADATA__DURATION);
        createEAttribute(videoMetadataEClass, VIDEO_METADATA__WIDTH);
        createEAttribute(videoMetadataEClass, VIDEO_METADATA__HEIGHT);

        // Create enums
        accessLevelEEnum = createEEnum(ACCESS_LEVEL);
        userRoleEEnum = createEEnum(USER_ROLE);
        hashAlgoEEnum = createEEnum(HASH_ALGO);
        libraryTypeEEnum = createEEnum(LIBRARY_TYPE);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private boolean isInitialized = false;

    /**
     * Complete the initialization of the package and its meta-model. This method is
     * guarded to have no affect on any invocation but its first. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public void initializePackageContents() {
        if (isInitialized)
            return;
        isInitialized = true;

        // Initialize package
        setName(eNAME);
        setNsPrefix(eNS_PREFIX);
        setNsURI(eNS_URI);

        // Create type parameters

        // Set bounds for type parameters

        // Add supertypes to classes
        userEClass.getESuperTypes().add(this.getEntity());
        libraryEClass.getESuperTypes().add(this.getEntity());
        libraryItemEClass.getESuperTypes().add(this.getEntity());
        commentEClass.getESuperTypes().add(this.getEntity());
        articleMetadataEClass.getESuperTypes().add(this.getContentMetadata());
        imageMetadataEClass.getESuperTypes().add(this.getContentMetadata());
        audioMetadataEClass.getESuperTypes().add(this.getContentMetadata());
        videoMetadataEClass.getESuperTypes().add(this.getContentMetadata());

        // Initialize classes, features, and operations; add parameters
        initEClass(entityEClass, Entity.class, "Entity", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getEntity_Id(), ecorePackage.getEString(), "id", null, 0, 1, Entity.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(userEClass, User.class, "User", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getUser_UserName(), ecorePackage.getEString(), "userName", null, 0, 1, User.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getUser_Credentials(), this.getCredentials(), null, "credentials", null, 1, 1, User.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
                IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getUser_Email(), ecorePackage.getEString(), "email", null, 0, 1, User.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getUser_Attributes(), this.getMemberAttribute(), null, "attributes", null, 0, -1, User.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
                IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getUser_Libraries(), this.getLibrary(), null, "libraries", null, 0, -1, User.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
                IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(credentialsEClass, Credentials.class, "Credentials", !IS_ABSTRACT, !IS_INTERFACE,
                IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getCredentials_Algo(), this.getHashAlgo(), "algo", null, 1, 1, Credentials.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getCredentials_Value(), ecorePackage.getEByteArray(), "value", null, 1, 1, Credentials.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(memberAttributeEClass, MemberAttribute.class, "MemberAttribute", !IS_ABSTRACT, !IS_INTERFACE,
                IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getMemberAttribute_Label(), ecorePackage.getEString(), "label", null, 1, 1,
                MemberAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
                !IS_DERIVED, IS_ORDERED);
        initEAttribute(getMemberAttribute_Content(), ecorePackage.getEString(), "content", null, 1, 1,
                MemberAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
                !IS_DERIVED, IS_ORDERED);

        initEClass(libraryEClass, Library.class, "Library", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getLibrary_Slug(), ecorePackage.getEString(), "slug", null, 1, 1, Library.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getLibrary_Name(), ecorePackage.getEString(), "name", null, 1, 1, Library.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getLibrary_Description(), ecorePackage.getEString(), "description", null, 0, 1, Library.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getLibrary_Type(), this.getLibraryType(), "type", null, 1, 1, Library.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getLibrary_CreatedDate(), ecorePackage.getEDate(), "createdDate", null, 1, 1, Library.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getLibrary_Access(), this.getAccessLevel(), "access", null, 1, 1, Library.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getLibrary_Items(), this.getLibraryItem(), null, "items", null, 0, -1, Library.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
                IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(libraryItemEClass, LibraryItem.class, "LibraryItem", !IS_ABSTRACT, !IS_INTERFACE,
                IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getLibraryItem_Title(), ecorePackage.getEString(), "title", null, 1, 1, LibraryItem.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getLibraryItem_Description(), ecorePackage.getEString(), "description", null, 0, 1,
                LibraryItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
                !IS_DERIVED, IS_ORDERED);
        initEReference(getLibraryItem_Comments(), this.getComment(), null, "comments", null, 0, -1, LibraryItem.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
                IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getLibraryItem_Metadata(), this.getContentMetadata(), null, "metadata", null, 1, 1,
                LibraryItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
                !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getLibraryItem_CreatedDate(), ecorePackage.getEDate(), "createdDate", null, 0, 1,
                LibraryItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
                !IS_DERIVED, IS_ORDERED);
        initEAttribute(getLibraryItem_Slug(), ecorePackage.getEString(), "slug", null, 1, 1, LibraryItem.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(commentEClass, Comment.class, "Comment", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
        initEReference(getComment_Replies(), this.getComment(), null, "replies", null, 0, -1, Comment.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
                IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEReference(getComment_Owner(), this.getUser(), null, "owner", null, 1, 1, Comment.class, !IS_TRANSIENT,
                !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
                IS_ORDERED);
        initEAttribute(getComment_Content(), ecorePackage.getEString(), "content", null, 1, 1, Comment.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getComment_CreatedDate(), ecorePackage.getEDate(), "createdDate", null, 1, 1, Comment.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(contentMetadataEClass, ContentMetadata.class, "ContentMetadata", IS_ABSTRACT, IS_INTERFACE,
                IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getContentMetadata_Uri(), ecorePackage.getEString(), "uri", null, 1, 1, ContentMetadata.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getContentMetadata_Mimetype(), ecorePackage.getEString(), "mimetype", null, 1, 1,
                ContentMetadata.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
                !IS_DERIVED, IS_ORDERED);

        initEClass(articleMetadataEClass, ArticleMetadata.class, "ArticleMetadata", !IS_ABSTRACT, !IS_INTERFACE,
                IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getArticleMetadata_Image(), ecorePackage.getEString(), "image", null, 0, 1,
                ArticleMetadata.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
                !IS_DERIVED, IS_ORDERED);

        initEClass(imageMetadataEClass, ImageMetadata.class, "ImageMetadata", !IS_ABSTRACT, !IS_INTERFACE,
                IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getImageMetadata_Width(), ecorePackage.getEInt(), "width", null, 0, 1, ImageMetadata.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getImageMetadata_Height(), ecorePackage.getEInt(), "height", null, 0, 1, ImageMetadata.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(audioMetadataEClass, AudioMetadata.class, "AudioMetadata", !IS_ABSTRACT, !IS_INTERFACE,
                IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getAudioMetadata_Duration(), ecorePackage.getEInt(), "duration", null, 0, 1, AudioMetadata.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        initEClass(videoMetadataEClass, VideoMetadata.class, "VideoMetadata", !IS_ABSTRACT, !IS_INTERFACE,
                IS_GENERATED_INSTANCE_CLASS);
        initEAttribute(getVideoMetadata_Duration(), ecorePackage.getEInt(), "duration", null, 0, 1, VideoMetadata.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getVideoMetadata_Width(), ecorePackage.getEInt(), "width", null, 0, 1, VideoMetadata.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
        initEAttribute(getVideoMetadata_Height(), ecorePackage.getEInt(), "height", null, 0, 1, VideoMetadata.class,
                !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

        // Initialize enums and add enum literals
        initEEnum(accessLevelEEnum, AccessLevel.class, "AccessLevel");
        addEEnumLiteral(accessLevelEEnum, AccessLevel.PRIVATE);
        addEEnumLiteral(accessLevelEEnum, AccessLevel.PUBLIC);

        initEEnum(userRoleEEnum, UserRole.class, "UserRole");
        addEEnumLiteral(userRoleEEnum, UserRole.ADMIN);
        addEEnumLiteral(userRoleEEnum, UserRole.SUPPORT);
        addEEnumLiteral(userRoleEEnum, UserRole.MEMBER);

        initEEnum(hashAlgoEEnum, HashAlgo.class, "HashAlgo");
        addEEnumLiteral(hashAlgoEEnum, HashAlgo.MD5);

        initEEnum(libraryTypeEEnum, LibraryType.class, "LibraryType");
        addEEnumLiteral(libraryTypeEEnum, LibraryType.DISCUSSION);
        addEEnumLiteral(libraryTypeEEnum, LibraryType.ALBUM);
        addEEnumLiteral(libraryTypeEEnum, LibraryType.GALLERY);

        // Create resource
        createResource(eNS_URI);
    }

} // ModelPackageImpl
