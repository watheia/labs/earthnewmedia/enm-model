/**
 */
package earthnewmedia.model.impl;

import java.util.Collection;
import java.util.Date;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import earthnewmedia.model.Comment;
import earthnewmedia.model.ContentMetadata;
import earthnewmedia.model.LibraryItem;
import earthnewmedia.model.ModelPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Library
 * Item</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link earthnewmedia.model.impl.LibraryItemImpl#getId <em>Id</em>}</li>
 * <li>{@link earthnewmedia.model.impl.LibraryItemImpl#getTitle
 * <em>Title</em>}</li>
 * <li>{@link earthnewmedia.model.impl.LibraryItemImpl#getDescription
 * <em>Description</em>}</li>
 * <li>{@link earthnewmedia.model.impl.LibraryItemImpl#getComments
 * <em>Comments</em>}</li>
 * <li>{@link earthnewmedia.model.impl.LibraryItemImpl#getMetadata
 * <em>Metadata</em>}</li>
 * <li>{@link earthnewmedia.model.impl.LibraryItemImpl#getCreatedDate
 * <em>Created Date</em>}</li>
 * <li>{@link earthnewmedia.model.impl.LibraryItemImpl#getSlug
 * <em>Slug</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LibraryItemImpl extends MinimalEObjectImpl.Container implements LibraryItem {
    /**
     * The default value of the '{@link #getId() <em>Id</em>}' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getId()
     * @generated
     * @ordered
     */
    protected static final String ID_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getId() <em>Id</em>}' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getId()
     * @generated
     * @ordered
     */
    protected String id = ID_EDEFAULT;

    /**
     * The default value of the '{@link #getTitle() <em>Title</em>}' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getTitle()
     * @generated
     * @ordered
     */
    protected static final String TITLE_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getTitle() <em>Title</em>}' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getTitle()
     * @generated
     * @ordered
     */
    protected String title = TITLE_EDEFAULT;

    /**
     * The default value of the '{@link #getDescription() <em>Description</em>}'
     * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getDescription()
     * @generated
     * @ordered
     */
    protected static final String DESCRIPTION_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getDescription() <em>Description</em>}'
     * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getDescription()
     * @generated
     * @ordered
     */
    protected String description = DESCRIPTION_EDEFAULT;

    /**
     * The cached value of the '{@link #getComments() <em>Comments</em>}'
     * containment reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getComments()
     * @generated
     * @ordered
     */
    protected EList<Comment> comments;

    /**
     * The cached value of the '{@link #getMetadata() <em>Metadata</em>}'
     * containment reference. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getMetadata()
     * @generated
     * @ordered
     */
    protected ContentMetadata metadata;

    /**
     * The default value of the '{@link #getCreatedDate() <em>Created Date</em>}'
     * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getCreatedDate()
     * @generated
     * @ordered
     */
    protected static final Date CREATED_DATE_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getCreatedDate() <em>Created Date</em>}'
     * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getCreatedDate()
     * @generated
     * @ordered
     */
    protected Date createdDate = CREATED_DATE_EDEFAULT;

    /**
     * The default value of the '{@link #getSlug() <em>Slug</em>}' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getSlug()
     * @generated
     * @ordered
     */
    protected static final String SLUG_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getSlug() <em>Slug</em>}' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getSlug()
     * @generated
     * @ordered
     */
    protected String slug = SLUG_EDEFAULT;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    protected LibraryItemImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ModelPackage.Literals.LIBRARY_ITEM;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getId() {
        return id;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setId(String newId) {
        String oldId = id;
        id = newId;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.LIBRARY_ITEM__ID, oldId, id));
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getTitle() {
        return title;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setTitle(String newTitle) {
        String oldTitle = title;
        title = newTitle;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.LIBRARY_ITEM__TITLE, oldTitle, title));
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getDescription() {
        return description;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setDescription(String newDescription) {
        String oldDescription = description;
        description = newDescription;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.LIBRARY_ITEM__DESCRIPTION,
                    oldDescription, description));
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<Comment> getComments() {
        if (comments == null) {
            comments = new EObjectContainmentEList<Comment>(Comment.class, this, ModelPackage.LIBRARY_ITEM__COMMENTS);
        }
        return comments;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public ContentMetadata getMetadata() {
        return metadata;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public NotificationChain basicSetMetadata(ContentMetadata newMetadata, NotificationChain msgs) {
        ContentMetadata oldMetadata = metadata;
        metadata = newMetadata;
        if (eNotificationRequired()) {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
                    ModelPackage.LIBRARY_ITEM__METADATA, oldMetadata, newMetadata);
            if (msgs == null)
                msgs = notification;
            else
                msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setMetadata(ContentMetadata newMetadata) {
        if (newMetadata != metadata) {
            NotificationChain msgs = null;
            if (metadata != null)
                msgs = ((InternalEObject) metadata).eInverseRemove(this,
                        EOPPOSITE_FEATURE_BASE - ModelPackage.LIBRARY_ITEM__METADATA, null, msgs);
            if (newMetadata != null)
                msgs = ((InternalEObject) newMetadata).eInverseAdd(this,
                        EOPPOSITE_FEATURE_BASE - ModelPackage.LIBRARY_ITEM__METADATA, null, msgs);
            msgs = basicSetMetadata(newMetadata, msgs);
            if (msgs != null)
                msgs.dispatch();
        } else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.LIBRARY_ITEM__METADATA, newMetadata,
                    newMetadata));
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Date getCreatedDate() {
        return createdDate;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setCreatedDate(Date newCreatedDate) {
        Date oldCreatedDate = createdDate;
        createdDate = newCreatedDate;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.LIBRARY_ITEM__CREATED_DATE,
                    oldCreatedDate, createdDate));
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getSlug() {
        return slug;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setSlug(String newSlug) {
        String oldSlug = slug;
        slug = newSlug;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.LIBRARY_ITEM__SLUG, oldSlug, slug));
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
        case ModelPackage.LIBRARY_ITEM__COMMENTS:
            return ((InternalEList<?>) getComments()).basicRemove(otherEnd, msgs);
        case ModelPackage.LIBRARY_ITEM__METADATA:
            return basicSetMetadata(null, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
        case ModelPackage.LIBRARY_ITEM__ID:
            return getId();
        case ModelPackage.LIBRARY_ITEM__TITLE:
            return getTitle();
        case ModelPackage.LIBRARY_ITEM__DESCRIPTION:
            return getDescription();
        case ModelPackage.LIBRARY_ITEM__COMMENTS:
            return getComments();
        case ModelPackage.LIBRARY_ITEM__METADATA:
            return getMetadata();
        case ModelPackage.LIBRARY_ITEM__CREATED_DATE:
            return getCreatedDate();
        case ModelPackage.LIBRARY_ITEM__SLUG:
            return getSlug();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    @SuppressWarnings("unchecked")
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
        case ModelPackage.LIBRARY_ITEM__ID:
            setId((String) newValue);
            return;
        case ModelPackage.LIBRARY_ITEM__TITLE:
            setTitle((String) newValue);
            return;
        case ModelPackage.LIBRARY_ITEM__DESCRIPTION:
            setDescription((String) newValue);
            return;
        case ModelPackage.LIBRARY_ITEM__COMMENTS:
            getComments().clear();
            getComments().addAll((Collection<? extends Comment>) newValue);
            return;
        case ModelPackage.LIBRARY_ITEM__METADATA:
            setMetadata((ContentMetadata) newValue);
            return;
        case ModelPackage.LIBRARY_ITEM__CREATED_DATE:
            setCreatedDate((Date) newValue);
            return;
        case ModelPackage.LIBRARY_ITEM__SLUG:
            setSlug((String) newValue);
            return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
        case ModelPackage.LIBRARY_ITEM__ID:
            setId(ID_EDEFAULT);
            return;
        case ModelPackage.LIBRARY_ITEM__TITLE:
            setTitle(TITLE_EDEFAULT);
            return;
        case ModelPackage.LIBRARY_ITEM__DESCRIPTION:
            setDescription(DESCRIPTION_EDEFAULT);
            return;
        case ModelPackage.LIBRARY_ITEM__COMMENTS:
            getComments().clear();
            return;
        case ModelPackage.LIBRARY_ITEM__METADATA:
            setMetadata((ContentMetadata) null);
            return;
        case ModelPackage.LIBRARY_ITEM__CREATED_DATE:
            setCreatedDate(CREATED_DATE_EDEFAULT);
            return;
        case ModelPackage.LIBRARY_ITEM__SLUG:
            setSlug(SLUG_EDEFAULT);
            return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
        case ModelPackage.LIBRARY_ITEM__ID:
            return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
        case ModelPackage.LIBRARY_ITEM__TITLE:
            return TITLE_EDEFAULT == null ? title != null : !TITLE_EDEFAULT.equals(title);
        case ModelPackage.LIBRARY_ITEM__DESCRIPTION:
            return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
        case ModelPackage.LIBRARY_ITEM__COMMENTS:
            return comments != null && !comments.isEmpty();
        case ModelPackage.LIBRARY_ITEM__METADATA:
            return metadata != null;
        case ModelPackage.LIBRARY_ITEM__CREATED_DATE:
            return CREATED_DATE_EDEFAULT == null ? createdDate != null : !CREATED_DATE_EDEFAULT.equals(createdDate);
        case ModelPackage.LIBRARY_ITEM__SLUG:
            return SLUG_EDEFAULT == null ? slug != null : !SLUG_EDEFAULT.equals(slug);
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy())
            return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (id: ");
        result.append(id);
        result.append(", title: ");
        result.append(title);
        result.append(", description: ");
        result.append(description);
        result.append(", createdDate: ");
        result.append(createdDate);
        result.append(", slug: ");
        result.append(slug);
        result.append(')');
        return result.toString();
    }

} // LibraryItemImpl
