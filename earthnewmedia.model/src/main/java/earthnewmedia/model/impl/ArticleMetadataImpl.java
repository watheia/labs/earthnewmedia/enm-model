/**
 */
package earthnewmedia.model.impl;

import earthnewmedia.model.ArticleMetadata;
import earthnewmedia.model.ModelPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Article
 * Metadata</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link earthnewmedia.model.impl.ArticleMetadataImpl#getUri
 * <em>Uri</em>}</li>
 * <li>{@link earthnewmedia.model.impl.ArticleMetadataImpl#getMimetype
 * <em>Mimetype</em>}</li>
 * <li>{@link earthnewmedia.model.impl.ArticleMetadataImpl#getImage
 * <em>Image</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ArticleMetadataImpl extends MinimalEObjectImpl.Container implements ArticleMetadata {
    /**
     * The default value of the '{@link #getUri() <em>Uri</em>}' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getUri()
     * @generated
     * @ordered
     */
    protected static final String URI_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getUri() <em>Uri</em>}' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getUri()
     * @generated
     * @ordered
     */
    protected String uri = URI_EDEFAULT;

    /**
     * The default value of the '{@link #getMimetype() <em>Mimetype</em>}'
     * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getMimetype()
     * @generated
     * @ordered
     */
    protected static final String MIMETYPE_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getMimetype() <em>Mimetype</em>}' attribute.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getMimetype()
     * @generated
     * @ordered
     */
    protected String mimetype = MIMETYPE_EDEFAULT;

    /**
     * The default value of the '{@link #getImage() <em>Image</em>}' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getImage()
     * @generated
     * @ordered
     */
    protected static final String IMAGE_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getImage() <em>Image</em>}' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getImage()
     * @generated
     * @ordered
     */
    protected String image = IMAGE_EDEFAULT;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    protected ArticleMetadataImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ModelPackage.Literals.ARTICLE_METADATA;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getUri() {
        return uri;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setUri(String newUri) {
        String oldUri = uri;
        uri = newUri;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.ARTICLE_METADATA__URI, oldUri, uri));
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getMimetype() {
        return mimetype;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setMimetype(String newMimetype) {
        String oldMimetype = mimetype;
        mimetype = newMimetype;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.ARTICLE_METADATA__MIMETYPE, oldMimetype,
                    mimetype));
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getImage() {
        return image;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setImage(String newImage) {
        String oldImage = image;
        image = newImage;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.ARTICLE_METADATA__IMAGE, oldImage,
                    image));
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
        case ModelPackage.ARTICLE_METADATA__URI:
            return getUri();
        case ModelPackage.ARTICLE_METADATA__MIMETYPE:
            return getMimetype();
        case ModelPackage.ARTICLE_METADATA__IMAGE:
            return getImage();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
        case ModelPackage.ARTICLE_METADATA__URI:
            setUri((String) newValue);
            return;
        case ModelPackage.ARTICLE_METADATA__MIMETYPE:
            setMimetype((String) newValue);
            return;
        case ModelPackage.ARTICLE_METADATA__IMAGE:
            setImage((String) newValue);
            return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
        case ModelPackage.ARTICLE_METADATA__URI:
            setUri(URI_EDEFAULT);
            return;
        case ModelPackage.ARTICLE_METADATA__MIMETYPE:
            setMimetype(MIMETYPE_EDEFAULT);
            return;
        case ModelPackage.ARTICLE_METADATA__IMAGE:
            setImage(IMAGE_EDEFAULT);
            return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
        case ModelPackage.ARTICLE_METADATA__URI:
            return URI_EDEFAULT == null ? uri != null : !URI_EDEFAULT.equals(uri);
        case ModelPackage.ARTICLE_METADATA__MIMETYPE:
            return MIMETYPE_EDEFAULT == null ? mimetype != null : !MIMETYPE_EDEFAULT.equals(mimetype);
        case ModelPackage.ARTICLE_METADATA__IMAGE:
            return IMAGE_EDEFAULT == null ? image != null : !IMAGE_EDEFAULT.equals(image);
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy())
            return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (uri: ");
        result.append(uri);
        result.append(", mimetype: ");
        result.append(mimetype);
        result.append(", image: ");
        result.append(image);
        result.append(')');
        return result.toString();
    }

} // ArticleMetadataImpl
