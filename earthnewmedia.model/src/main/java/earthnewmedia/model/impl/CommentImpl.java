/**
 */
package earthnewmedia.model.impl;

import java.util.Collection;
import java.util.Date;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import earthnewmedia.model.Comment;
import earthnewmedia.model.ModelPackage;
import earthnewmedia.model.User;

/**
 * <!-- begin-user-doc --> An implementation of the model object
 * '<em><b>Comment</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link earthnewmedia.model.impl.CommentImpl#getId <em>Id</em>}</li>
 * <li>{@link earthnewmedia.model.impl.CommentImpl#getReplies
 * <em>Replies</em>}</li>
 * <li>{@link earthnewmedia.model.impl.CommentImpl#getOwner <em>Owner</em>}</li>
 * <li>{@link earthnewmedia.model.impl.CommentImpl#getContent
 * <em>Content</em>}</li>
 * <li>{@link earthnewmedia.model.impl.CommentImpl#getCreatedDate <em>Created
 * Date</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CommentImpl extends MinimalEObjectImpl.Container implements Comment {
    /**
     * The default value of the '{@link #getId() <em>Id</em>}' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getId()
     * @generated
     * @ordered
     */
    protected static final String ID_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getId() <em>Id</em>}' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getId()
     * @generated
     * @ordered
     */
    protected String id = ID_EDEFAULT;

    /**
     * The cached value of the '{@link #getReplies() <em>Replies</em>}' containment
     * reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getReplies()
     * @generated
     * @ordered
     */
    protected EList<Comment> replies;

    /**
     * The cached value of the '{@link #getOwner() <em>Owner</em>}' reference. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getOwner()
     * @generated
     * @ordered
     */
    protected User owner;

    /**
     * The default value of the '{@link #getContent() <em>Content</em>}' attribute.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getContent()
     * @generated
     * @ordered
     */
    protected static final String CONTENT_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getContent() <em>Content</em>}' attribute.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getContent()
     * @generated
     * @ordered
     */
    protected String content = CONTENT_EDEFAULT;

    /**
     * The default value of the '{@link #getCreatedDate() <em>Created Date</em>}'
     * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getCreatedDate()
     * @generated
     * @ordered
     */
    protected static final Date CREATED_DATE_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getCreatedDate() <em>Created Date</em>}'
     * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getCreatedDate()
     * @generated
     * @ordered
     */
    protected Date createdDate = CREATED_DATE_EDEFAULT;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    protected CommentImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ModelPackage.Literals.COMMENT;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getId() {
        return id;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setId(String newId) {
        String oldId = id;
        id = newId;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.COMMENT__ID, oldId, id));
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<Comment> getReplies() {
        if (replies == null) {
            replies = new EObjectContainmentEList<Comment>(Comment.class, this, ModelPackage.COMMENT__REPLIES);
        }
        return replies;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public User getOwner() {
        if (owner != null && owner.eIsProxy()) {
            InternalEObject oldOwner = (InternalEObject) owner;
            owner = (User) eResolveProxy(oldOwner);
            if (owner != oldOwner) {
                if (eNotificationRequired())
                    eNotify(new ENotificationImpl(this, Notification.RESOLVE, ModelPackage.COMMENT__OWNER, oldOwner,
                            owner));
            }
        }
        return owner;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public User basicGetOwner() {
        return owner;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setOwner(User newOwner) {
        User oldOwner = owner;
        owner = newOwner;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.COMMENT__OWNER, oldOwner, owner));
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getContent() {
        return content;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setContent(String newContent) {
        String oldContent = content;
        content = newContent;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.COMMENT__CONTENT, oldContent, content));
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Date getCreatedDate() {
        return createdDate;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setCreatedDate(Date newCreatedDate) {
        Date oldCreatedDate = createdDate;
        createdDate = newCreatedDate;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.COMMENT__CREATED_DATE, oldCreatedDate,
                    createdDate));
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
        case ModelPackage.COMMENT__REPLIES:
            return ((InternalEList<?>) getReplies()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
        case ModelPackage.COMMENT__ID:
            return getId();
        case ModelPackage.COMMENT__REPLIES:
            return getReplies();
        case ModelPackage.COMMENT__OWNER:
            if (resolve)
                return getOwner();
            return basicGetOwner();
        case ModelPackage.COMMENT__CONTENT:
            return getContent();
        case ModelPackage.COMMENT__CREATED_DATE:
            return getCreatedDate();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    @SuppressWarnings("unchecked")
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
        case ModelPackage.COMMENT__ID:
            setId((String) newValue);
            return;
        case ModelPackage.COMMENT__REPLIES:
            getReplies().clear();
            getReplies().addAll((Collection<? extends Comment>) newValue);
            return;
        case ModelPackage.COMMENT__OWNER:
            setOwner((User) newValue);
            return;
        case ModelPackage.COMMENT__CONTENT:
            setContent((String) newValue);
            return;
        case ModelPackage.COMMENT__CREATED_DATE:
            setCreatedDate((Date) newValue);
            return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
        case ModelPackage.COMMENT__ID:
            setId(ID_EDEFAULT);
            return;
        case ModelPackage.COMMENT__REPLIES:
            getReplies().clear();
            return;
        case ModelPackage.COMMENT__OWNER:
            setOwner((User) null);
            return;
        case ModelPackage.COMMENT__CONTENT:
            setContent(CONTENT_EDEFAULT);
            return;
        case ModelPackage.COMMENT__CREATED_DATE:
            setCreatedDate(CREATED_DATE_EDEFAULT);
            return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
        case ModelPackage.COMMENT__ID:
            return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
        case ModelPackage.COMMENT__REPLIES:
            return replies != null && !replies.isEmpty();
        case ModelPackage.COMMENT__OWNER:
            return owner != null;
        case ModelPackage.COMMENT__CONTENT:
            return CONTENT_EDEFAULT == null ? content != null : !CONTENT_EDEFAULT.equals(content);
        case ModelPackage.COMMENT__CREATED_DATE:
            return CREATED_DATE_EDEFAULT == null ? createdDate != null : !CREATED_DATE_EDEFAULT.equals(createdDate);
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy())
            return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (id: ");
        result.append(id);
        result.append(", content: ");
        result.append(content);
        result.append(", createdDate: ");
        result.append(createdDate);
        result.append(')');
        return result.toString();
    }

} // CommentImpl
