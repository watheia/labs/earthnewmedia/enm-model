/**
 */
package earthnewmedia.model.impl;

import earthnewmedia.model.ModelPackage;
import earthnewmedia.model.VideoMetadata;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc --> An implementation of the model object '<em><b>Video
 * Metadata</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link earthnewmedia.model.impl.VideoMetadataImpl#getUri
 * <em>Uri</em>}</li>
 * <li>{@link earthnewmedia.model.impl.VideoMetadataImpl#getMimetype
 * <em>Mimetype</em>}</li>
 * <li>{@link earthnewmedia.model.impl.VideoMetadataImpl#getDuration
 * <em>Duration</em>}</li>
 * <li>{@link earthnewmedia.model.impl.VideoMetadataImpl#getWidth
 * <em>Width</em>}</li>
 * <li>{@link earthnewmedia.model.impl.VideoMetadataImpl#getHeight
 * <em>Height</em>}</li>
 * </ul>
 *
 * @generated
 */
public class VideoMetadataImpl extends MinimalEObjectImpl.Container implements VideoMetadata {
    /**
     * The default value of the '{@link #getUri() <em>Uri</em>}' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getUri()
     * @generated
     * @ordered
     */
    protected static final String URI_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getUri() <em>Uri</em>}' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getUri()
     * @generated
     * @ordered
     */
    protected String uri = URI_EDEFAULT;

    /**
     * The default value of the '{@link #getMimetype() <em>Mimetype</em>}'
     * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getMimetype()
     * @generated
     * @ordered
     */
    protected static final String MIMETYPE_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getMimetype() <em>Mimetype</em>}' attribute.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getMimetype()
     * @generated
     * @ordered
     */
    protected String mimetype = MIMETYPE_EDEFAULT;

    /**
     * The default value of the '{@link #getDuration() <em>Duration</em>}'
     * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getDuration()
     * @generated
     * @ordered
     */
    protected static final int DURATION_EDEFAULT = 0;

    /**
     * The cached value of the '{@link #getDuration() <em>Duration</em>}' attribute.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getDuration()
     * @generated
     * @ordered
     */
    protected int duration = DURATION_EDEFAULT;

    /**
     * The default value of the '{@link #getWidth() <em>Width</em>}' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getWidth()
     * @generated
     * @ordered
     */
    protected static final int WIDTH_EDEFAULT = 0;

    /**
     * The cached value of the '{@link #getWidth() <em>Width</em>}' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getWidth()
     * @generated
     * @ordered
     */
    protected int width = WIDTH_EDEFAULT;

    /**
     * The default value of the '{@link #getHeight() <em>Height</em>}' attribute.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getHeight()
     * @generated
     * @ordered
     */
    protected static final int HEIGHT_EDEFAULT = 0;

    /**
     * The cached value of the '{@link #getHeight() <em>Height</em>}' attribute.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getHeight()
     * @generated
     * @ordered
     */
    protected int height = HEIGHT_EDEFAULT;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    protected VideoMetadataImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ModelPackage.Literals.VIDEO_METADATA;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getUri() {
        return uri;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setUri(String newUri) {
        String oldUri = uri;
        uri = newUri;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.VIDEO_METADATA__URI, oldUri, uri));
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getMimetype() {
        return mimetype;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setMimetype(String newMimetype) {
        String oldMimetype = mimetype;
        mimetype = newMimetype;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.VIDEO_METADATA__MIMETYPE, oldMimetype,
                    mimetype));
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public int getDuration() {
        return duration;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setDuration(int newDuration) {
        int oldDuration = duration;
        duration = newDuration;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.VIDEO_METADATA__DURATION, oldDuration,
                    duration));
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public int getWidth() {
        return width;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setWidth(int newWidth) {
        int oldWidth = width;
        width = newWidth;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.VIDEO_METADATA__WIDTH, oldWidth, width));
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public int getHeight() {
        return height;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setHeight(int newHeight) {
        int oldHeight = height;
        height = newHeight;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.VIDEO_METADATA__HEIGHT, oldHeight,
                    height));
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
        case ModelPackage.VIDEO_METADATA__URI:
            return getUri();
        case ModelPackage.VIDEO_METADATA__MIMETYPE:
            return getMimetype();
        case ModelPackage.VIDEO_METADATA__DURATION:
            return getDuration();
        case ModelPackage.VIDEO_METADATA__WIDTH:
            return getWidth();
        case ModelPackage.VIDEO_METADATA__HEIGHT:
            return getHeight();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
        case ModelPackage.VIDEO_METADATA__URI:
            setUri((String) newValue);
            return;
        case ModelPackage.VIDEO_METADATA__MIMETYPE:
            setMimetype((String) newValue);
            return;
        case ModelPackage.VIDEO_METADATA__DURATION:
            setDuration((Integer) newValue);
            return;
        case ModelPackage.VIDEO_METADATA__WIDTH:
            setWidth((Integer) newValue);
            return;
        case ModelPackage.VIDEO_METADATA__HEIGHT:
            setHeight((Integer) newValue);
            return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
        case ModelPackage.VIDEO_METADATA__URI:
            setUri(URI_EDEFAULT);
            return;
        case ModelPackage.VIDEO_METADATA__MIMETYPE:
            setMimetype(MIMETYPE_EDEFAULT);
            return;
        case ModelPackage.VIDEO_METADATA__DURATION:
            setDuration(DURATION_EDEFAULT);
            return;
        case ModelPackage.VIDEO_METADATA__WIDTH:
            setWidth(WIDTH_EDEFAULT);
            return;
        case ModelPackage.VIDEO_METADATA__HEIGHT:
            setHeight(HEIGHT_EDEFAULT);
            return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
        case ModelPackage.VIDEO_METADATA__URI:
            return URI_EDEFAULT == null ? uri != null : !URI_EDEFAULT.equals(uri);
        case ModelPackage.VIDEO_METADATA__MIMETYPE:
            return MIMETYPE_EDEFAULT == null ? mimetype != null : !MIMETYPE_EDEFAULT.equals(mimetype);
        case ModelPackage.VIDEO_METADATA__DURATION:
            return duration != DURATION_EDEFAULT;
        case ModelPackage.VIDEO_METADATA__WIDTH:
            return width != WIDTH_EDEFAULT;
        case ModelPackage.VIDEO_METADATA__HEIGHT:
            return height != HEIGHT_EDEFAULT;
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy())
            return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (uri: ");
        result.append(uri);
        result.append(", mimetype: ");
        result.append(mimetype);
        result.append(", duration: ");
        result.append(duration);
        result.append(", width: ");
        result.append(width);
        result.append(", height: ");
        result.append(height);
        result.append(')');
        return result.toString();
    }

} // VideoMetadataImpl
