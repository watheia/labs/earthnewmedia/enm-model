/**
 */
package earthnewmedia.model.impl;

import earthnewmedia.model.Credentials;
import earthnewmedia.model.HashAlgo;
import earthnewmedia.model.ModelPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc --> An implementation of the model object
 * '<em><b>Credentials</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link earthnewmedia.model.impl.CredentialsImpl#getAlgo
 * <em>Algo</em>}</li>
 * <li>{@link earthnewmedia.model.impl.CredentialsImpl#getValue
 * <em>Value</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CredentialsImpl extends MinimalEObjectImpl.Container implements Credentials {
    /**
     * The default value of the '{@link #getAlgo() <em>Algo</em>}' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getAlgo()
     * @generated
     * @ordered
     */
    protected static final HashAlgo ALGO_EDEFAULT = HashAlgo.MD5;

    /**
     * The cached value of the '{@link #getAlgo() <em>Algo</em>}' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getAlgo()
     * @generated
     * @ordered
     */
    protected HashAlgo algo = ALGO_EDEFAULT;

    /**
     * The default value of the '{@link #getValue() <em>Value</em>}' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getValue()
     * @generated
     * @ordered
     */
    protected static final byte[] VALUE_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getValue() <em>Value</em>}' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getValue()
     * @generated
     * @ordered
     */
    protected byte[] value = VALUE_EDEFAULT;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    protected CredentialsImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ModelPackage.Literals.CREDENTIALS;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public HashAlgo getAlgo() {
        return algo;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setAlgo(HashAlgo newAlgo) {
        HashAlgo oldAlgo = algo;
        algo = newAlgo == null ? ALGO_EDEFAULT : newAlgo;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.CREDENTIALS__ALGO, oldAlgo, algo));
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public byte[] getValue() {
        return value;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setValue(byte[] newValue) {
        byte[] oldValue = value;
        value = newValue;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.CREDENTIALS__VALUE, oldValue, value));
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
        case ModelPackage.CREDENTIALS__ALGO:
            return getAlgo();
        case ModelPackage.CREDENTIALS__VALUE:
            return getValue();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
        case ModelPackage.CREDENTIALS__ALGO:
            setAlgo((HashAlgo) newValue);
            return;
        case ModelPackage.CREDENTIALS__VALUE:
            setValue((byte[]) newValue);
            return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
        case ModelPackage.CREDENTIALS__ALGO:
            setAlgo(ALGO_EDEFAULT);
            return;
        case ModelPackage.CREDENTIALS__VALUE:
            setValue(VALUE_EDEFAULT);
            return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
        case ModelPackage.CREDENTIALS__ALGO:
            return algo != ALGO_EDEFAULT;
        case ModelPackage.CREDENTIALS__VALUE:
            return VALUE_EDEFAULT == null ? value != null : !VALUE_EDEFAULT.equals(value);
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy())
            return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (algo: ");
        result.append(algo);
        result.append(", value: ");
        result.append(value);
        result.append(')');
        return result.toString();
    }

} // CredentialsImpl
