/**
 */
package earthnewmedia.model.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import earthnewmedia.model.Credentials;
import earthnewmedia.model.Library;
import earthnewmedia.model.MemberAttribute;
import earthnewmedia.model.ModelPackage;
import earthnewmedia.model.User;

/**
 * <!-- begin-user-doc --> An implementation of the model object
 * '<em><b>User</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link earthnewmedia.model.impl.UserImpl#getId <em>Id</em>}</li>
 * <li>{@link earthnewmedia.model.impl.UserImpl#getUserName <em>User
 * Name</em>}</li>
 * <li>{@link earthnewmedia.model.impl.UserImpl#getCredentials
 * <em>Credentials</em>}</li>
 * <li>{@link earthnewmedia.model.impl.UserImpl#getEmail <em>Email</em>}</li>
 * <li>{@link earthnewmedia.model.impl.UserImpl#getAttributes
 * <em>Attributes</em>}</li>
 * <li>{@link earthnewmedia.model.impl.UserImpl#getLibraries
 * <em>Libraries</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UserImpl extends MinimalEObjectImpl.Container implements User {
    /**
     * The default value of the '{@link #getId() <em>Id</em>}' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getId()
     * @generated
     * @ordered
     */
    protected static final String ID_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getId() <em>Id</em>}' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getId()
     * @generated
     * @ordered
     */
    protected String id = ID_EDEFAULT;

    /**
     * The default value of the '{@link #getUserName() <em>User Name</em>}'
     * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getUserName()
     * @generated
     * @ordered
     */
    protected static final String USER_NAME_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getUserName() <em>User Name</em>}'
     * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getUserName()
     * @generated
     * @ordered
     */
    protected String userName = USER_NAME_EDEFAULT;

    /**
     * The cached value of the '{@link #getCredentials() <em>Credentials</em>}'
     * containment reference. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getCredentials()
     * @generated
     * @ordered
     */
    protected Credentials credentials;

    /**
     * The default value of the '{@link #getEmail() <em>Email</em>}' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getEmail()
     * @generated
     * @ordered
     */
    protected static final String EMAIL_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getEmail() <em>Email</em>}' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getEmail()
     * @generated
     * @ordered
     */
    protected String email = EMAIL_EDEFAULT;

    /**
     * The cached value of the '{@link #getAttributes() <em>Attributes</em>}'
     * containment reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getAttributes()
     * @generated
     * @ordered
     */
    protected EList<MemberAttribute> attributes;

    /**
     * The cached value of the '{@link #getLibraries() <em>Libraries</em>}'
     * containment reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getLibraries()
     * @generated
     * @ordered
     */
    protected EList<Library> libraries;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    protected UserImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ModelPackage.Literals.USER;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getId() {
        return id;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setId(String newId) {
        String oldId = id;
        id = newId;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.USER__ID, oldId, id));
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getUserName() {
        return userName;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setUserName(String newUserName) {
        String oldUserName = userName;
        userName = newUserName;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.USER__USER_NAME, oldUserName, userName));
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Credentials getCredentials() {
        return credentials;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public NotificationChain basicSetCredentials(Credentials newCredentials, NotificationChain msgs) {
        Credentials oldCredentials = credentials;
        credentials = newCredentials;
        if (eNotificationRequired()) {
            ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
                    ModelPackage.USER__CREDENTIALS, oldCredentials, newCredentials);
            if (msgs == null)
                msgs = notification;
            else
                msgs.add(notification);
        }
        return msgs;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setCredentials(Credentials newCredentials) {
        if (newCredentials != credentials) {
            NotificationChain msgs = null;
            if (credentials != null)
                msgs = ((InternalEObject) credentials).eInverseRemove(this,
                        EOPPOSITE_FEATURE_BASE - ModelPackage.USER__CREDENTIALS, null, msgs);
            if (newCredentials != null)
                msgs = ((InternalEObject) newCredentials).eInverseAdd(this,
                        EOPPOSITE_FEATURE_BASE - ModelPackage.USER__CREDENTIALS, null, msgs);
            msgs = basicSetCredentials(newCredentials, msgs);
            if (msgs != null)
                msgs.dispatch();
        } else if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.USER__CREDENTIALS, newCredentials,
                    newCredentials));
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getEmail() {
        return email;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setEmail(String newEmail) {
        String oldEmail = email;
        email = newEmail;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.USER__EMAIL, oldEmail, email));
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<MemberAttribute> getAttributes() {
        if (attributes == null) {
            attributes = new EObjectContainmentEList<MemberAttribute>(MemberAttribute.class, this,
                    ModelPackage.USER__ATTRIBUTES);
        }
        return attributes;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<Library> getLibraries() {
        if (libraries == null) {
            libraries = new EObjectContainmentEList<Library>(Library.class, this, ModelPackage.USER__LIBRARIES);
        }
        return libraries;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
        case ModelPackage.USER__CREDENTIALS:
            return basicSetCredentials(null, msgs);
        case ModelPackage.USER__ATTRIBUTES:
            return ((InternalEList<?>) getAttributes()).basicRemove(otherEnd, msgs);
        case ModelPackage.USER__LIBRARIES:
            return ((InternalEList<?>) getLibraries()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
        case ModelPackage.USER__ID:
            return getId();
        case ModelPackage.USER__USER_NAME:
            return getUserName();
        case ModelPackage.USER__CREDENTIALS:
            return getCredentials();
        case ModelPackage.USER__EMAIL:
            return getEmail();
        case ModelPackage.USER__ATTRIBUTES:
            return getAttributes();
        case ModelPackage.USER__LIBRARIES:
            return getLibraries();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    @SuppressWarnings("unchecked")
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
        case ModelPackage.USER__ID:
            setId((String) newValue);
            return;
        case ModelPackage.USER__USER_NAME:
            setUserName((String) newValue);
            return;
        case ModelPackage.USER__CREDENTIALS:
            setCredentials((Credentials) newValue);
            return;
        case ModelPackage.USER__EMAIL:
            setEmail((String) newValue);
            return;
        case ModelPackage.USER__ATTRIBUTES:
            getAttributes().clear();
            getAttributes().addAll((Collection<? extends MemberAttribute>) newValue);
            return;
        case ModelPackage.USER__LIBRARIES:
            getLibraries().clear();
            getLibraries().addAll((Collection<? extends Library>) newValue);
            return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
        case ModelPackage.USER__ID:
            setId(ID_EDEFAULT);
            return;
        case ModelPackage.USER__USER_NAME:
            setUserName(USER_NAME_EDEFAULT);
            return;
        case ModelPackage.USER__CREDENTIALS:
            setCredentials((Credentials) null);
            return;
        case ModelPackage.USER__EMAIL:
            setEmail(EMAIL_EDEFAULT);
            return;
        case ModelPackage.USER__ATTRIBUTES:
            getAttributes().clear();
            return;
        case ModelPackage.USER__LIBRARIES:
            getLibraries().clear();
            return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
        case ModelPackage.USER__ID:
            return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
        case ModelPackage.USER__USER_NAME:
            return USER_NAME_EDEFAULT == null ? userName != null : !USER_NAME_EDEFAULT.equals(userName);
        case ModelPackage.USER__CREDENTIALS:
            return credentials != null;
        case ModelPackage.USER__EMAIL:
            return EMAIL_EDEFAULT == null ? email != null : !EMAIL_EDEFAULT.equals(email);
        case ModelPackage.USER__ATTRIBUTES:
            return attributes != null && !attributes.isEmpty();
        case ModelPackage.USER__LIBRARIES:
            return libraries != null && !libraries.isEmpty();
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy())
            return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (id: ");
        result.append(id);
        result.append(", userName: ");
        result.append(userName);
        result.append(", email: ");
        result.append(email);
        result.append(')');
        return result.toString();
    }

} // UserImpl
