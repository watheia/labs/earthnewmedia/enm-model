/**
 */
package earthnewmedia.model.impl;

import java.util.Collection;
import java.util.Date;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import earthnewmedia.model.AccessLevel;
import earthnewmedia.model.Library;
import earthnewmedia.model.LibraryItem;
import earthnewmedia.model.LibraryType;
import earthnewmedia.model.ModelPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object
 * '<em><b>Library</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link earthnewmedia.model.impl.LibraryImpl#getId <em>Id</em>}</li>
 * <li>{@link earthnewmedia.model.impl.LibraryImpl#getSlug <em>Slug</em>}</li>
 * <li>{@link earthnewmedia.model.impl.LibraryImpl#getName <em>Name</em>}</li>
 * <li>{@link earthnewmedia.model.impl.LibraryImpl#getDescription
 * <em>Description</em>}</li>
 * <li>{@link earthnewmedia.model.impl.LibraryImpl#getType <em>Type</em>}</li>
 * <li>{@link earthnewmedia.model.impl.LibraryImpl#getCreatedDate <em>Created
 * Date</em>}</li>
 * <li>{@link earthnewmedia.model.impl.LibraryImpl#getAccess
 * <em>Access</em>}</li>
 * <li>{@link earthnewmedia.model.impl.LibraryImpl#getItems <em>Items</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LibraryImpl extends MinimalEObjectImpl.Container implements Library {
    /**
     * The default value of the '{@link #getId() <em>Id</em>}' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getId()
     * @generated
     * @ordered
     */
    protected static final String ID_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getId() <em>Id</em>}' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getId()
     * @generated
     * @ordered
     */
    protected String id = ID_EDEFAULT;

    /**
     * The default value of the '{@link #getSlug() <em>Slug</em>}' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getSlug()
     * @generated
     * @ordered
     */
    protected static final String SLUG_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getSlug() <em>Slug</em>}' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getSlug()
     * @generated
     * @ordered
     */
    protected String slug = SLUG_EDEFAULT;

    /**
     * The default value of the '{@link #getName() <em>Name</em>}' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getName()
     * @generated
     * @ordered
     */
    protected static final String NAME_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getName() <em>Name</em>}' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getName()
     * @generated
     * @ordered
     */
    protected String name = NAME_EDEFAULT;

    /**
     * The default value of the '{@link #getDescription() <em>Description</em>}'
     * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getDescription()
     * @generated
     * @ordered
     */
    protected static final String DESCRIPTION_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getDescription() <em>Description</em>}'
     * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getDescription()
     * @generated
     * @ordered
     */
    protected String description = DESCRIPTION_EDEFAULT;

    /**
     * The default value of the '{@link #getType() <em>Type</em>}' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getType()
     * @generated
     * @ordered
     */
    protected static final LibraryType TYPE_EDEFAULT = LibraryType.DISCUSSION;

    /**
     * The cached value of the '{@link #getType() <em>Type</em>}' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getType()
     * @generated
     * @ordered
     */
    protected LibraryType type = TYPE_EDEFAULT;

    /**
     * The default value of the '{@link #getCreatedDate() <em>Created Date</em>}'
     * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getCreatedDate()
     * @generated
     * @ordered
     */
    protected static final Date CREATED_DATE_EDEFAULT = null;

    /**
     * The cached value of the '{@link #getCreatedDate() <em>Created Date</em>}'
     * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getCreatedDate()
     * @generated
     * @ordered
     */
    protected Date createdDate = CREATED_DATE_EDEFAULT;

    /**
     * The default value of the '{@link #getAccess() <em>Access</em>}' attribute.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getAccess()
     * @generated
     * @ordered
     */
    protected static final AccessLevel ACCESS_EDEFAULT = AccessLevel.PRIVATE;

    /**
     * The cached value of the '{@link #getAccess() <em>Access</em>}' attribute.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getAccess()
     * @generated
     * @ordered
     */
    protected AccessLevel access = ACCESS_EDEFAULT;

    /**
     * The cached value of the '{@link #getItems() <em>Items</em>}' containment
     * reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see #getItems()
     * @generated
     * @ordered
     */
    protected EList<LibraryItem> items;

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    protected LibraryImpl() {
        super();
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected EClass eStaticClass() {
        return ModelPackage.Literals.LIBRARY;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getId() {
        return id;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setId(String newId) {
        String oldId = id;
        id = newId;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.LIBRARY__ID, oldId, id));
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getSlug() {
        return slug;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setSlug(String newSlug) {
        String oldSlug = slug;
        slug = newSlug;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.LIBRARY__SLUG, oldSlug, slug));
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setName(String newName) {
        String oldName = name;
        name = newName;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.LIBRARY__NAME, oldName, name));
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String getDescription() {
        return description;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setDescription(String newDescription) {
        String oldDescription = description;
        description = newDescription;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.LIBRARY__DESCRIPTION, oldDescription,
                    description));
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public LibraryType getType() {
        return type;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setType(LibraryType newType) {
        LibraryType oldType = type;
        type = newType == null ? TYPE_EDEFAULT : newType;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.LIBRARY__TYPE, oldType, type));
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Date getCreatedDate() {
        return createdDate;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setCreatedDate(Date newCreatedDate) {
        Date oldCreatedDate = createdDate;
        createdDate = newCreatedDate;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.LIBRARY__CREATED_DATE, oldCreatedDate,
                    createdDate));
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public AccessLevel getAccess() {
        return access;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void setAccess(AccessLevel newAccess) {
        AccessLevel oldAccess = access;
        access = newAccess == null ? ACCESS_EDEFAULT : newAccess;
        if (eNotificationRequired())
            eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.LIBRARY__ACCESS, oldAccess, access));
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public EList<LibraryItem> getItems() {
        if (items == null) {
            items = new EObjectContainmentEList<LibraryItem>(LibraryItem.class, this, ModelPackage.LIBRARY__ITEMS);
        }
        return items;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
        switch (featureID) {
        case ModelPackage.LIBRARY__ITEMS:
            return ((InternalEList<?>) getItems()).basicRemove(otherEnd, msgs);
        }
        return super.eInverseRemove(otherEnd, featureID, msgs);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public Object eGet(int featureID, boolean resolve, boolean coreType) {
        switch (featureID) {
        case ModelPackage.LIBRARY__ID:
            return getId();
        case ModelPackage.LIBRARY__SLUG:
            return getSlug();
        case ModelPackage.LIBRARY__NAME:
            return getName();
        case ModelPackage.LIBRARY__DESCRIPTION:
            return getDescription();
        case ModelPackage.LIBRARY__TYPE:
            return getType();
        case ModelPackage.LIBRARY__CREATED_DATE:
            return getCreatedDate();
        case ModelPackage.LIBRARY__ACCESS:
            return getAccess();
        case ModelPackage.LIBRARY__ITEMS:
            return getItems();
        }
        return super.eGet(featureID, resolve, coreType);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    @SuppressWarnings("unchecked")
    public void eSet(int featureID, Object newValue) {
        switch (featureID) {
        case ModelPackage.LIBRARY__ID:
            setId((String) newValue);
            return;
        case ModelPackage.LIBRARY__SLUG:
            setSlug((String) newValue);
            return;
        case ModelPackage.LIBRARY__NAME:
            setName((String) newValue);
            return;
        case ModelPackage.LIBRARY__DESCRIPTION:
            setDescription((String) newValue);
            return;
        case ModelPackage.LIBRARY__TYPE:
            setType((LibraryType) newValue);
            return;
        case ModelPackage.LIBRARY__CREATED_DATE:
            setCreatedDate((Date) newValue);
            return;
        case ModelPackage.LIBRARY__ACCESS:
            setAccess((AccessLevel) newValue);
            return;
        case ModelPackage.LIBRARY__ITEMS:
            getItems().clear();
            getItems().addAll((Collection<? extends LibraryItem>) newValue);
            return;
        }
        super.eSet(featureID, newValue);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public void eUnset(int featureID) {
        switch (featureID) {
        case ModelPackage.LIBRARY__ID:
            setId(ID_EDEFAULT);
            return;
        case ModelPackage.LIBRARY__SLUG:
            setSlug(SLUG_EDEFAULT);
            return;
        case ModelPackage.LIBRARY__NAME:
            setName(NAME_EDEFAULT);
            return;
        case ModelPackage.LIBRARY__DESCRIPTION:
            setDescription(DESCRIPTION_EDEFAULT);
            return;
        case ModelPackage.LIBRARY__TYPE:
            setType(TYPE_EDEFAULT);
            return;
        case ModelPackage.LIBRARY__CREATED_DATE:
            setCreatedDate(CREATED_DATE_EDEFAULT);
            return;
        case ModelPackage.LIBRARY__ACCESS:
            setAccess(ACCESS_EDEFAULT);
            return;
        case ModelPackage.LIBRARY__ITEMS:
            getItems().clear();
            return;
        }
        super.eUnset(featureID);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public boolean eIsSet(int featureID) {
        switch (featureID) {
        case ModelPackage.LIBRARY__ID:
            return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
        case ModelPackage.LIBRARY__SLUG:
            return SLUG_EDEFAULT == null ? slug != null : !SLUG_EDEFAULT.equals(slug);
        case ModelPackage.LIBRARY__NAME:
            return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
        case ModelPackage.LIBRARY__DESCRIPTION:
            return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
        case ModelPackage.LIBRARY__TYPE:
            return type != TYPE_EDEFAULT;
        case ModelPackage.LIBRARY__CREATED_DATE:
            return CREATED_DATE_EDEFAULT == null ? createdDate != null : !CREATED_DATE_EDEFAULT.equals(createdDate);
        case ModelPackage.LIBRARY__ACCESS:
            return access != ACCESS_EDEFAULT;
        case ModelPackage.LIBRARY__ITEMS:
            return items != null && !items.isEmpty();
        }
        return super.eIsSet(featureID);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public String toString() {
        if (eIsProxy())
            return super.toString();

        StringBuilder result = new StringBuilder(super.toString());
        result.append(" (id: ");
        result.append(id);
        result.append(", slug: ");
        result.append(slug);
        result.append(", name: ");
        result.append(name);
        result.append(", description: ");
        result.append(description);
        result.append(", type: ");
        result.append(type);
        result.append(", createdDate: ");
        result.append(createdDate);
        result.append(", access: ");
        result.append(access);
        result.append(')');
        return result.toString();
    }

} // LibraryImpl
