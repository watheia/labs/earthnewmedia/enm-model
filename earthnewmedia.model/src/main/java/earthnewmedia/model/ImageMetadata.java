/**
 */
package earthnewmedia.model;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Image
 * Metadata</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link earthnewmedia.model.ImageMetadata#getWidth <em>Width</em>}</li>
 * <li>{@link earthnewmedia.model.ImageMetadata#getHeight <em>Height</em>}</li>
 * </ul>
 *
 * @see earthnewmedia.model.ModelPackage#getImageMetadata()
 * @model
 * @generated
 */
public interface ImageMetadata extends ContentMetadata {
    /**
     * Returns the value of the '<em><b>Width</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Width</em>' attribute.
     * @see #setWidth(int)
     * @see earthnewmedia.model.ModelPackage#getImageMetadata_Width()
     * @model
     * @generated
     */
    int getWidth();

    /**
     * Sets the value of the '{@link earthnewmedia.model.ImageMetadata#getWidth
     * <em>Width</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Width</em>' attribute.
     * @see #getWidth()
     * @generated
     */
    void setWidth(int value);

    /**
     * Returns the value of the '<em><b>Height</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Height</em>' attribute.
     * @see #setHeight(int)
     * @see earthnewmedia.model.ModelPackage#getImageMetadata_Height()
     * @model
     * @generated
     */
    int getHeight();

    /**
     * Sets the value of the '{@link earthnewmedia.model.ImageMetadata#getHeight
     * <em>Height</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Height</em>' attribute.
     * @see #getHeight()
     * @generated
     */
    void setHeight(int value);

} // ImageMetadata
