/**
 */
package earthnewmedia.model;

import java.util.Date;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object
 * '<em><b>Comment</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link earthnewmedia.model.Comment#getReplies <em>Replies</em>}</li>
 * <li>{@link earthnewmedia.model.Comment#getOwner <em>Owner</em>}</li>
 * <li>{@link earthnewmedia.model.Comment#getContent <em>Content</em>}</li>
 * <li>{@link earthnewmedia.model.Comment#getCreatedDate <em>Created
 * Date</em>}</li>
 * </ul>
 *
 * @see earthnewmedia.model.ModelPackage#getComment()
 * @model
 * @generated
 */
public interface Comment extends Entity {
    /**
     * Returns the value of the '<em><b>Replies</b></em>' containment reference
     * list. The list contents are of type {@link earthnewmedia.model.Comment}. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Replies</em>' containment reference list.
     * @see earthnewmedia.model.ModelPackage#getComment_Replies()
     * @model containment="true"
     * @generated
     */
    EList<Comment> getReplies();

    /**
     * Returns the value of the '<em><b>Owner</b></em>' reference. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Owner</em>' reference.
     * @see #setOwner(User)
     * @see earthnewmedia.model.ModelPackage#getComment_Owner()
     * @model required="true"
     * @generated
     */
    User getOwner();

    /**
     * Sets the value of the '{@link earthnewmedia.model.Comment#getOwner
     * <em>Owner</em>}' reference. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Owner</em>' reference.
     * @see #getOwner()
     * @generated
     */
    void setOwner(User value);

    /**
     * Returns the value of the '<em><b>Content</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Content</em>' attribute.
     * @see #setContent(String)
     * @see earthnewmedia.model.ModelPackage#getComment_Content()
     * @model required="true"
     * @generated
     */
    String getContent();

    /**
     * Sets the value of the '{@link earthnewmedia.model.Comment#getContent
     * <em>Content</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Content</em>' attribute.
     * @see #getContent()
     * @generated
     */
    void setContent(String value);

    /**
     * Returns the value of the '<em><b>Created Date</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Created Date</em>' attribute.
     * @see #setCreatedDate(Date)
     * @see earthnewmedia.model.ModelPackage#getComment_CreatedDate()
     * @model required="true"
     * @generated
     */
    Date getCreatedDate();

    /**
     * Sets the value of the '{@link earthnewmedia.model.Comment#getCreatedDate
     * <em>Created Date</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
     * -->
     * 
     * @param value the new value of the '<em>Created Date</em>' attribute.
     * @see #getCreatedDate()
     * @generated
     */
    void setCreatedDate(Date value);

} // Comment
