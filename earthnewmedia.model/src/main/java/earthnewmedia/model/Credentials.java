/**
 */
package earthnewmedia.model;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object
 * '<em><b>Credentials</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link earthnewmedia.model.Credentials#getAlgo <em>Algo</em>}</li>
 * <li>{@link earthnewmedia.model.Credentials#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see earthnewmedia.model.ModelPackage#getCredentials()
 * @model
 * @generated
 */
public interface Credentials extends EObject {
    /**
     * Returns the value of the '<em><b>Algo</b></em>' attribute. The literals are
     * from the enumeration {@link earthnewmedia.model.HashAlgo}. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Algo</em>' attribute.
     * @see earthnewmedia.model.HashAlgo
     * @see #setAlgo(HashAlgo)
     * @see earthnewmedia.model.ModelPackage#getCredentials_Algo()
     * @model required="true"
     * @generated
     */
    HashAlgo getAlgo();

    /**
     * Sets the value of the '{@link earthnewmedia.model.Credentials#getAlgo
     * <em>Algo</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Algo</em>' attribute.
     * @see earthnewmedia.model.HashAlgo
     * @see #getAlgo()
     * @generated
     */
    void setAlgo(HashAlgo value);

    /**
     * Returns the value of the '<em><b>Value</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Value</em>' attribute.
     * @see #setValue(byte[])
     * @see earthnewmedia.model.ModelPackage#getCredentials_Value()
     * @model required="true"
     * @generated
     */
    byte[] getValue();

    /**
     * Sets the value of the '{@link earthnewmedia.model.Credentials#getValue
     * <em>Value</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Value</em>' attribute.
     * @see #getValue()
     * @generated
     */
    void setValue(byte[] value);

} // Credentials
