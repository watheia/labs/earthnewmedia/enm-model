/**
 */
package earthnewmedia.model;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object
 * '<em><b>User</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link earthnewmedia.model.User#getUserName <em>User Name</em>}</li>
 * <li>{@link earthnewmedia.model.User#getCredentials <em>Credentials</em>}</li>
 * <li>{@link earthnewmedia.model.User#getEmail <em>Email</em>}</li>
 * <li>{@link earthnewmedia.model.User#getAttributes <em>Attributes</em>}</li>
 * <li>{@link earthnewmedia.model.User#getLibraries <em>Libraries</em>}</li>
 * </ul>
 *
 * @see earthnewmedia.model.ModelPackage#getUser()
 * @model
 * @generated
 */
public interface User extends Entity {
    /**
     * Returns the value of the '<em><b>User Name</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>User Name</em>' attribute.
     * @see #setUserName(String)
     * @see earthnewmedia.model.ModelPackage#getUser_UserName()
     * @model
     * @generated
     */
    String getUserName();

    /**
     * Sets the value of the '{@link earthnewmedia.model.User#getUserName <em>User
     * Name</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>User Name</em>' attribute.
     * @see #getUserName()
     * @generated
     */
    void setUserName(String value);

    /**
     * Returns the value of the '<em><b>Credentials</b></em>' containment reference.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Credentials</em>' containment reference.
     * @see #setCredentials(Credentials)
     * @see earthnewmedia.model.ModelPackage#getUser_Credentials()
     * @model containment="true" required="true"
     * @generated
     */
    Credentials getCredentials();

    /**
     * Sets the value of the '{@link earthnewmedia.model.User#getCredentials
     * <em>Credentials</em>}' containment reference. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @param value the new value of the '<em>Credentials</em>' containment
     *              reference.
     * @see #getCredentials()
     * @generated
     */
    void setCredentials(Credentials value);

    /**
     * Returns the value of the '<em><b>Email</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Email</em>' attribute.
     * @see #setEmail(String)
     * @see earthnewmedia.model.ModelPackage#getUser_Email()
     * @model
     * @generated
     */
    String getEmail();

    /**
     * Sets the value of the '{@link earthnewmedia.model.User#getEmail
     * <em>Email</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Email</em>' attribute.
     * @see #getEmail()
     * @generated
     */
    void setEmail(String value);

    /**
     * Returns the value of the '<em><b>Attributes</b></em>' containment reference
     * list. The list contents are of type
     * {@link earthnewmedia.model.MemberAttribute}. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @return the value of the '<em>Attributes</em>' containment reference list.
     * @see earthnewmedia.model.ModelPackage#getUser_Attributes()
     * @model containment="true"
     * @generated
     */
    EList<MemberAttribute> getAttributes();

    /**
     * Returns the value of the '<em><b>Libraries</b></em>' containment reference
     * list. The list contents are of type {@link earthnewmedia.model.Library}. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Libraries</em>' containment reference list.
     * @see earthnewmedia.model.ModelPackage#getUser_Libraries()
     * @model containment="true"
     * @generated
     */
    EList<Library> getLibraries();

} // User
