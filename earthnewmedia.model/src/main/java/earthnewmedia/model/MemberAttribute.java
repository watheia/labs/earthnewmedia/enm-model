/**
 */
package earthnewmedia.model;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> A representation of the model object '<em><b>Member
 * Attribute</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link earthnewmedia.model.MemberAttribute#getLabel <em>Label</em>}</li>
 * <li>{@link earthnewmedia.model.MemberAttribute#getContent
 * <em>Content</em>}</li>
 * </ul>
 *
 * @see earthnewmedia.model.ModelPackage#getMemberAttribute()
 * @model
 * @generated
 */
public interface MemberAttribute extends EObject {
    /**
     * Returns the value of the '<em><b>Label</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Label</em>' attribute.
     * @see #setLabel(String)
     * @see earthnewmedia.model.ModelPackage#getMemberAttribute_Label()
     * @model required="true"
     * @generated
     */
    String getLabel();

    /**
     * Sets the value of the '{@link earthnewmedia.model.MemberAttribute#getLabel
     * <em>Label</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Label</em>' attribute.
     * @see #getLabel()
     * @generated
     */
    void setLabel(String value);

    /**
     * Returns the value of the '<em><b>Content</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Content</em>' attribute.
     * @see #setContent(String)
     * @see earthnewmedia.model.ModelPackage#getMemberAttribute_Content()
     * @model required="true"
     * @generated
     */
    String getContent();

    /**
     * Sets the value of the '{@link earthnewmedia.model.MemberAttribute#getContent
     * <em>Content</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Content</em>' attribute.
     * @see #getContent()
     * @generated
     */
    void setContent(String value);

} // MemberAttribute
