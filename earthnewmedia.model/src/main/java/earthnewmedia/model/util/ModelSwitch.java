/**
 */
package earthnewmedia.model.util;

import earthnewmedia.model.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc --> The <b>Switch</b> for the model's inheritance
 * hierarchy. It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object and proceeding up the
 * inheritance hierarchy until a non-null result is returned, which is the
 * result of the switch. <!-- end-user-doc -->
 * 
 * @see earthnewmedia.model.ModelPackage
 * @generated
 */
public class ModelSwitch<T> extends Switch<T> {
    /**
     * The cached model package <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    protected static ModelPackage modelPackage;

    /**
     * Creates an instance of the switch. <!-- begin-user-doc --> <!-- end-user-doc
     * -->
     * 
     * @generated
     */
    public ModelSwitch() {
        if (modelPackage == null) {
            modelPackage = ModelPackage.eINSTANCE;
        }
    }

    /**
     * Checks whether this is a switch for the given package. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @param ePackage the package in question.
     * @return whether this is a switch for the given package.
     * @generated
     */
    @Override
    protected boolean isSwitchFor(EPackage ePackage) {
        return ePackage == modelPackage;
    }

    /**
     * Calls <code>caseXXX</code> for each class of the model until one returns a
     * non null result; it yields that result. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @return the first non-null result returned by a <code>caseXXX</code> call.
     * @generated
     */
    @Override
    protected T doSwitch(int classifierID, EObject theEObject) {
        switch (classifierID) {
        case ModelPackage.ENTITY: {
            Entity entity = (Entity) theEObject;
            T result = caseEntity(entity);
            if (result == null)
                result = defaultCase(theEObject);
            return result;
        }
        case ModelPackage.USER: {
            User user = (User) theEObject;
            T result = caseUser(user);
            if (result == null)
                result = caseEntity(user);
            if (result == null)
                result = defaultCase(theEObject);
            return result;
        }
        case ModelPackage.CREDENTIALS: {
            Credentials credentials = (Credentials) theEObject;
            T result = caseCredentials(credentials);
            if (result == null)
                result = defaultCase(theEObject);
            return result;
        }
        case ModelPackage.MEMBER_ATTRIBUTE: {
            MemberAttribute memberAttribute = (MemberAttribute) theEObject;
            T result = caseMemberAttribute(memberAttribute);
            if (result == null)
                result = defaultCase(theEObject);
            return result;
        }
        case ModelPackage.LIBRARY: {
            Library library = (Library) theEObject;
            T result = caseLibrary(library);
            if (result == null)
                result = caseEntity(library);
            if (result == null)
                result = defaultCase(theEObject);
            return result;
        }
        case ModelPackage.LIBRARY_ITEM: {
            LibraryItem libraryItem = (LibraryItem) theEObject;
            T result = caseLibraryItem(libraryItem);
            if (result == null)
                result = caseEntity(libraryItem);
            if (result == null)
                result = defaultCase(theEObject);
            return result;
        }
        case ModelPackage.COMMENT: {
            Comment comment = (Comment) theEObject;
            T result = caseComment(comment);
            if (result == null)
                result = caseEntity(comment);
            if (result == null)
                result = defaultCase(theEObject);
            return result;
        }
        case ModelPackage.CONTENT_METADATA: {
            ContentMetadata contentMetadata = (ContentMetadata) theEObject;
            T result = caseContentMetadata(contentMetadata);
            if (result == null)
                result = defaultCase(theEObject);
            return result;
        }
        case ModelPackage.ARTICLE_METADATA: {
            ArticleMetadata articleMetadata = (ArticleMetadata) theEObject;
            T result = caseArticleMetadata(articleMetadata);
            if (result == null)
                result = caseContentMetadata(articleMetadata);
            if (result == null)
                result = defaultCase(theEObject);
            return result;
        }
        case ModelPackage.IMAGE_METADATA: {
            ImageMetadata imageMetadata = (ImageMetadata) theEObject;
            T result = caseImageMetadata(imageMetadata);
            if (result == null)
                result = caseContentMetadata(imageMetadata);
            if (result == null)
                result = defaultCase(theEObject);
            return result;
        }
        case ModelPackage.AUDIO_METADATA: {
            AudioMetadata audioMetadata = (AudioMetadata) theEObject;
            T result = caseAudioMetadata(audioMetadata);
            if (result == null)
                result = caseContentMetadata(audioMetadata);
            if (result == null)
                result = defaultCase(theEObject);
            return result;
        }
        case ModelPackage.VIDEO_METADATA: {
            VideoMetadata videoMetadata = (VideoMetadata) theEObject;
            T result = caseVideoMetadata(videoMetadata);
            if (result == null)
                result = caseContentMetadata(videoMetadata);
            if (result == null)
                result = defaultCase(theEObject);
            return result;
        }
        default:
            return defaultCase(theEObject);
        }
    }

    /**
     * Returns the result of interpreting the object as an instance of
     * '<em>Entity</em>'. <!-- begin-user-doc --> This implementation returns null;
     * returning a non-null result will terminate the switch. <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of
     *         '<em>Entity</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseEntity(Entity object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of
     * '<em>User</em>'. <!-- begin-user-doc --> This implementation returns null;
     * returning a non-null result will terminate the switch. <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of
     *         '<em>User</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseUser(User object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of
     * '<em>Credentials</em>'. <!-- begin-user-doc --> This implementation returns
     * null; returning a non-null result will terminate the switch. <!--
     * end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of
     *         '<em>Credentials</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseCredentials(Credentials object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Member
     * Attribute</em>'. <!-- begin-user-doc --> This implementation returns null;
     * returning a non-null result will terminate the switch. <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Member
     *         Attribute</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseMemberAttribute(MemberAttribute object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of
     * '<em>Library</em>'. <!-- begin-user-doc --> This implementation returns null;
     * returning a non-null result will terminate the switch. <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of
     *         '<em>Library</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseLibrary(Library object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Library
     * Item</em>'. <!-- begin-user-doc --> This implementation returns null;
     * returning a non-null result will terminate the switch. <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Library
     *         Item</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseLibraryItem(LibraryItem object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of
     * '<em>Comment</em>'. <!-- begin-user-doc --> This implementation returns null;
     * returning a non-null result will terminate the switch. <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of
     *         '<em>Comment</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseComment(Comment object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Content
     * Metadata</em>'. <!-- begin-user-doc --> This implementation returns null;
     * returning a non-null result will terminate the switch. <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Content
     *         Metadata</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseContentMetadata(ContentMetadata object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Article
     * Metadata</em>'. <!-- begin-user-doc --> This implementation returns null;
     * returning a non-null result will terminate the switch. <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Article
     *         Metadata</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseArticleMetadata(ArticleMetadata object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Image
     * Metadata</em>'. <!-- begin-user-doc --> This implementation returns null;
     * returning a non-null result will terminate the switch. <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Image
     *         Metadata</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseImageMetadata(ImageMetadata object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Audio
     * Metadata</em>'. <!-- begin-user-doc --> This implementation returns null;
     * returning a non-null result will terminate the switch. <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Audio
     *         Metadata</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseAudioMetadata(AudioMetadata object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of '<em>Video
     * Metadata</em>'. <!-- begin-user-doc --> This implementation returns null;
     * returning a non-null result will terminate the switch. <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of '<em>Video
     *         Metadata</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
     * @generated
     */
    public T caseVideoMetadata(VideoMetadata object) {
        return null;
    }

    /**
     * Returns the result of interpreting the object as an instance of
     * '<em>EObject</em>'. <!-- begin-user-doc --> This implementation returns null;
     * returning a non-null result will terminate the switch, but this is the last
     * case anyway. <!-- end-user-doc -->
     * 
     * @param object the target of the switch.
     * @return the result of interpreting the object as an instance of
     *         '<em>EObject</em>'.
     * @see #doSwitch(org.eclipse.emf.ecore.EObject)
     * @generated
     */
    @Override
    public T defaultCase(EObject object) {
        return null;
    }

} // ModelSwitch
