/**
 */
package earthnewmedia.model.util;

import earthnewmedia.model.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc --> The <b>Adapter Factory</b> for the model. It provides
 * an adapter <code>createXXX</code> method for each class of the model. <!--
 * end-user-doc -->
 * 
 * @see earthnewmedia.model.ModelPackage
 * @generated
 */
public class ModelAdapterFactory extends AdapterFactoryImpl {
    /**
     * The cached model package. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    protected static ModelPackage modelPackage;

    /**
     * Creates an instance of the adapter factory. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     */
    public ModelAdapterFactory() {
        if (modelPackage == null) {
            modelPackage = ModelPackage.eINSTANCE;
        }
    }

    /**
     * Returns whether this factory is applicable for the type of the object. <!--
     * begin-user-doc --> This implementation returns <code>true</code> if the
     * object is either the model's package or is an instance object of the model.
     * <!-- end-user-doc -->
     * 
     * @return whether this factory is applicable for the type of the object.
     * @generated
     */
    @Override
    public boolean isFactoryForType(Object object) {
        if (object == modelPackage) {
            return true;
        }
        if (object instanceof EObject) {
            return ((EObject) object).eClass().getEPackage() == modelPackage;
        }
        return false;
    }

    /**
     * The switch that delegates to the <code>createXXX</code> methods. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    protected ModelSwitch<Adapter> modelSwitch = new ModelSwitch<Adapter>() {
        @Override
        public Adapter caseEntity(Entity object) {
            return createEntityAdapter();
        }

        @Override
        public Adapter caseUser(User object) {
            return createUserAdapter();
        }

        @Override
        public Adapter caseCredentials(Credentials object) {
            return createCredentialsAdapter();
        }

        @Override
        public Adapter caseMemberAttribute(MemberAttribute object) {
            return createMemberAttributeAdapter();
        }

        @Override
        public Adapter caseLibrary(Library object) {
            return createLibraryAdapter();
        }

        @Override
        public Adapter caseLibraryItem(LibraryItem object) {
            return createLibraryItemAdapter();
        }

        @Override
        public Adapter caseComment(Comment object) {
            return createCommentAdapter();
        }

        @Override
        public Adapter caseContentMetadata(ContentMetadata object) {
            return createContentMetadataAdapter();
        }

        @Override
        public Adapter caseArticleMetadata(ArticleMetadata object) {
            return createArticleMetadataAdapter();
        }

        @Override
        public Adapter caseImageMetadata(ImageMetadata object) {
            return createImageMetadataAdapter();
        }

        @Override
        public Adapter caseAudioMetadata(AudioMetadata object) {
            return createAudioMetadataAdapter();
        }

        @Override
        public Adapter caseVideoMetadata(VideoMetadata object) {
            return createVideoMetadataAdapter();
        }

        @Override
        public Adapter defaultCase(EObject object) {
            return createEObjectAdapter();
        }
    };

    /**
     * Creates an adapter for the <code>target</code>. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @param target the object to adapt.
     * @return the adapter for the <code>target</code>.
     * @generated
     */
    @Override
    public Adapter createAdapter(Notifier target) {
        return modelSwitch.doSwitch((EObject) target);
    }

    /**
     * Creates a new adapter for an object of class
     * '{@link earthnewmedia.model.Entity <em>Entity</em>}'. <!-- begin-user-doc -->
     * This default implementation returns null so that we can easily ignore cases;
     * it's useful to ignore a case when inheritance will catch all the cases
     * anyway. <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see earthnewmedia.model.Entity
     * @generated
     */
    public Adapter createEntityAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class '{@link earthnewmedia.model.User
     * <em>User</em>}'. <!-- begin-user-doc --> This default implementation returns
     * null so that we can easily ignore cases; it's useful to ignore a case when
     * inheritance will catch all the cases anyway. <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see earthnewmedia.model.User
     * @generated
     */
    public Adapter createUserAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class
     * '{@link earthnewmedia.model.Credentials <em>Credentials</em>}'. <!--
     * begin-user-doc --> This default implementation returns null so that we can
     * easily ignore cases; it's useful to ignore a case when inheritance will catch
     * all the cases anyway. <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see earthnewmedia.model.Credentials
     * @generated
     */
    public Adapter createCredentialsAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class
     * '{@link earthnewmedia.model.MemberAttribute <em>Member Attribute</em>}'. <!--
     * begin-user-doc --> This default implementation returns null so that we can
     * easily ignore cases; it's useful to ignore a case when inheritance will catch
     * all the cases anyway. <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see earthnewmedia.model.MemberAttribute
     * @generated
     */
    public Adapter createMemberAttributeAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class
     * '{@link earthnewmedia.model.Library <em>Library</em>}'. <!-- begin-user-doc
     * --> This default implementation returns null so that we can easily ignore
     * cases; it's useful to ignore a case when inheritance will catch all the cases
     * anyway. <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see earthnewmedia.model.Library
     * @generated
     */
    public Adapter createLibraryAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class
     * '{@link earthnewmedia.model.LibraryItem <em>Library Item</em>}'. <!--
     * begin-user-doc --> This default implementation returns null so that we can
     * easily ignore cases; it's useful to ignore a case when inheritance will catch
     * all the cases anyway. <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see earthnewmedia.model.LibraryItem
     * @generated
     */
    public Adapter createLibraryItemAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class
     * '{@link earthnewmedia.model.Comment <em>Comment</em>}'. <!-- begin-user-doc
     * --> This default implementation returns null so that we can easily ignore
     * cases; it's useful to ignore a case when inheritance will catch all the cases
     * anyway. <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see earthnewmedia.model.Comment
     * @generated
     */
    public Adapter createCommentAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class
     * '{@link earthnewmedia.model.ContentMetadata <em>Content Metadata</em>}'. <!--
     * begin-user-doc --> This default implementation returns null so that we can
     * easily ignore cases; it's useful to ignore a case when inheritance will catch
     * all the cases anyway. <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see earthnewmedia.model.ContentMetadata
     * @generated
     */
    public Adapter createContentMetadataAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class
     * '{@link earthnewmedia.model.ArticleMetadata <em>Article Metadata</em>}'. <!--
     * begin-user-doc --> This default implementation returns null so that we can
     * easily ignore cases; it's useful to ignore a case when inheritance will catch
     * all the cases anyway. <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see earthnewmedia.model.ArticleMetadata
     * @generated
     */
    public Adapter createArticleMetadataAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class
     * '{@link earthnewmedia.model.ImageMetadata <em>Image Metadata</em>}'. <!--
     * begin-user-doc --> This default implementation returns null so that we can
     * easily ignore cases; it's useful to ignore a case when inheritance will catch
     * all the cases anyway. <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see earthnewmedia.model.ImageMetadata
     * @generated
     */
    public Adapter createImageMetadataAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class
     * '{@link earthnewmedia.model.AudioMetadata <em>Audio Metadata</em>}'. <!--
     * begin-user-doc --> This default implementation returns null so that we can
     * easily ignore cases; it's useful to ignore a case when inheritance will catch
     * all the cases anyway. <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see earthnewmedia.model.AudioMetadata
     * @generated
     */
    public Adapter createAudioMetadataAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for an object of class
     * '{@link earthnewmedia.model.VideoMetadata <em>Video Metadata</em>}'. <!--
     * begin-user-doc --> This default implementation returns null so that we can
     * easily ignore cases; it's useful to ignore a case when inheritance will catch
     * all the cases anyway. <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @see earthnewmedia.model.VideoMetadata
     * @generated
     */
    public Adapter createVideoMetadataAdapter() {
        return null;
    }

    /**
     * Creates a new adapter for the default case. <!-- begin-user-doc --> This
     * default implementation returns null. <!-- end-user-doc -->
     * 
     * @return the new adapter.
     * @generated
     */
    public Adapter createEObjectAdapter() {
        return null;
    }

} // ModelAdapterFactory
