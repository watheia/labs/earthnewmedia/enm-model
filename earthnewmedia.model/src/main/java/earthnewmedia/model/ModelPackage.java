/**
 */
package earthnewmedia.model;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc --> The <b>Package</b> for the model. It contains
 * accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each operation of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * 
 * @see earthnewmedia.model.ModelFactory
 * @model kind="package"
 * @generated
 */
public interface ModelPackage extends EPackage {
    /**
     * The package name. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNAME = "model";

    /**
     * The package namespace URI. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNS_URI = "http://www.earthnew.media/model";

    /**
     * The package namespace name. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    String eNS_PREFIX = "earthnewmedia.model";

    /**
     * The singleton instance of the package. <!-- begin-user-doc --> <!--
     * end-user-doc -->
     * 
     * @generated
     */
    ModelPackage eINSTANCE = earthnewmedia.model.impl.ModelPackageImpl.init();

    /**
     * The meta object id for the '{@link earthnewmedia.model.Entity
     * <em>Entity</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see earthnewmedia.model.Entity
     * @see earthnewmedia.model.impl.ModelPackageImpl#getEntity()
     * @generated
     */
    int ENTITY = 0;

    /**
     * The feature id for the '<em><b>Id</b></em>' attribute. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ENTITY__ID = 0;

    /**
     * The number of structural features of the '<em>Entity</em>' class. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ENTITY_FEATURE_COUNT = 1;

    /**
     * The number of operations of the '<em>Entity</em>' class. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ENTITY_OPERATION_COUNT = 0;

    /**
     * The meta object id for the '{@link earthnewmedia.model.impl.UserImpl
     * <em>User</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see earthnewmedia.model.impl.UserImpl
     * @see earthnewmedia.model.impl.ModelPackageImpl#getUser()
     * @generated
     */
    int USER = 1;

    /**
     * The feature id for the '<em><b>Id</b></em>' attribute. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int USER__ID = ENTITY__ID;

    /**
     * The feature id for the '<em><b>User Name</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int USER__USER_NAME = ENTITY_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Credentials</b></em>' containment reference.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int USER__CREDENTIALS = ENTITY_FEATURE_COUNT + 1;

    /**
     * The feature id for the '<em><b>Email</b></em>' attribute. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int USER__EMAIL = ENTITY_FEATURE_COUNT + 2;

    /**
     * The feature id for the '<em><b>Attributes</b></em>' containment reference
     * list. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int USER__ATTRIBUTES = ENTITY_FEATURE_COUNT + 3;

    /**
     * The feature id for the '<em><b>Libraries</b></em>' containment reference
     * list. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int USER__LIBRARIES = ENTITY_FEATURE_COUNT + 4;

    /**
     * The number of structural features of the '<em>User</em>' class. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int USER_FEATURE_COUNT = ENTITY_FEATURE_COUNT + 5;

    /**
     * The number of operations of the '<em>User</em>' class. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int USER_OPERATION_COUNT = ENTITY_OPERATION_COUNT + 0;

    /**
     * The meta object id for the '{@link earthnewmedia.model.impl.CredentialsImpl
     * <em>Credentials</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see earthnewmedia.model.impl.CredentialsImpl
     * @see earthnewmedia.model.impl.ModelPackageImpl#getCredentials()
     * @generated
     */
    int CREDENTIALS = 2;

    /**
     * The feature id for the '<em><b>Algo</b></em>' attribute. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CREDENTIALS__ALGO = 0;

    /**
     * The feature id for the '<em><b>Value</b></em>' attribute. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CREDENTIALS__VALUE = 1;

    /**
     * The number of structural features of the '<em>Credentials</em>' class. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CREDENTIALS_FEATURE_COUNT = 2;

    /**
     * The number of operations of the '<em>Credentials</em>' class. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CREDENTIALS_OPERATION_COUNT = 0;

    /**
     * The meta object id for the
     * '{@link earthnewmedia.model.impl.MemberAttributeImpl <em>Member
     * Attribute</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see earthnewmedia.model.impl.MemberAttributeImpl
     * @see earthnewmedia.model.impl.ModelPackageImpl#getMemberAttribute()
     * @generated
     */
    int MEMBER_ATTRIBUTE = 3;

    /**
     * The feature id for the '<em><b>Label</b></em>' attribute. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int MEMBER_ATTRIBUTE__LABEL = 0;

    /**
     * The feature id for the '<em><b>Content</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int MEMBER_ATTRIBUTE__CONTENT = 1;

    /**
     * The number of structural features of the '<em>Member Attribute</em>' class.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int MEMBER_ATTRIBUTE_FEATURE_COUNT = 2;

    /**
     * The number of operations of the '<em>Member Attribute</em>' class. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int MEMBER_ATTRIBUTE_OPERATION_COUNT = 0;

    /**
     * The meta object id for the '{@link earthnewmedia.model.impl.LibraryImpl
     * <em>Library</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see earthnewmedia.model.impl.LibraryImpl
     * @see earthnewmedia.model.impl.ModelPackageImpl#getLibrary()
     * @generated
     */
    int LIBRARY = 4;

    /**
     * The feature id for the '<em><b>Id</b></em>' attribute. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LIBRARY__ID = ENTITY__ID;

    /**
     * The feature id for the '<em><b>Slug</b></em>' attribute. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LIBRARY__SLUG = ENTITY_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Name</b></em>' attribute. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LIBRARY__NAME = ENTITY_FEATURE_COUNT + 1;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LIBRARY__DESCRIPTION = ENTITY_FEATURE_COUNT + 2;

    /**
     * The feature id for the '<em><b>Type</b></em>' attribute. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LIBRARY__TYPE = ENTITY_FEATURE_COUNT + 3;

    /**
     * The feature id for the '<em><b>Created Date</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LIBRARY__CREATED_DATE = ENTITY_FEATURE_COUNT + 4;

    /**
     * The feature id for the '<em><b>Access</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LIBRARY__ACCESS = ENTITY_FEATURE_COUNT + 5;

    /**
     * The feature id for the '<em><b>Items</b></em>' containment reference list.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LIBRARY__ITEMS = ENTITY_FEATURE_COUNT + 6;

    /**
     * The number of structural features of the '<em>Library</em>' class. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LIBRARY_FEATURE_COUNT = ENTITY_FEATURE_COUNT + 7;

    /**
     * The number of operations of the '<em>Library</em>' class. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LIBRARY_OPERATION_COUNT = ENTITY_OPERATION_COUNT + 0;

    /**
     * The meta object id for the '{@link earthnewmedia.model.impl.LibraryItemImpl
     * <em>Library Item</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see earthnewmedia.model.impl.LibraryItemImpl
     * @see earthnewmedia.model.impl.ModelPackageImpl#getLibraryItem()
     * @generated
     */
    int LIBRARY_ITEM = 5;

    /**
     * The feature id for the '<em><b>Id</b></em>' attribute. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LIBRARY_ITEM__ID = ENTITY__ID;

    /**
     * The feature id for the '<em><b>Title</b></em>' attribute. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LIBRARY_ITEM__TITLE = ENTITY_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Description</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LIBRARY_ITEM__DESCRIPTION = ENTITY_FEATURE_COUNT + 1;

    /**
     * The feature id for the '<em><b>Comments</b></em>' containment reference list.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LIBRARY_ITEM__COMMENTS = ENTITY_FEATURE_COUNT + 2;

    /**
     * The feature id for the '<em><b>Metadata</b></em>' containment reference. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LIBRARY_ITEM__METADATA = ENTITY_FEATURE_COUNT + 3;

    /**
     * The feature id for the '<em><b>Created Date</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LIBRARY_ITEM__CREATED_DATE = ENTITY_FEATURE_COUNT + 4;

    /**
     * The feature id for the '<em><b>Slug</b></em>' attribute. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LIBRARY_ITEM__SLUG = ENTITY_FEATURE_COUNT + 5;

    /**
     * The number of structural features of the '<em>Library Item</em>' class. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LIBRARY_ITEM_FEATURE_COUNT = ENTITY_FEATURE_COUNT + 6;

    /**
     * The number of operations of the '<em>Library Item</em>' class. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int LIBRARY_ITEM_OPERATION_COUNT = ENTITY_OPERATION_COUNT + 0;

    /**
     * The meta object id for the '{@link earthnewmedia.model.impl.CommentImpl
     * <em>Comment</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see earthnewmedia.model.impl.CommentImpl
     * @see earthnewmedia.model.impl.ModelPackageImpl#getComment()
     * @generated
     */
    int COMMENT = 6;

    /**
     * The feature id for the '<em><b>Id</b></em>' attribute. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int COMMENT__ID = ENTITY__ID;

    /**
     * The feature id for the '<em><b>Replies</b></em>' containment reference list.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int COMMENT__REPLIES = ENTITY_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Owner</b></em>' reference. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int COMMENT__OWNER = ENTITY_FEATURE_COUNT + 1;

    /**
     * The feature id for the '<em><b>Content</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int COMMENT__CONTENT = ENTITY_FEATURE_COUNT + 2;

    /**
     * The feature id for the '<em><b>Created Date</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int COMMENT__CREATED_DATE = ENTITY_FEATURE_COUNT + 3;

    /**
     * The number of structural features of the '<em>Comment</em>' class. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int COMMENT_FEATURE_COUNT = ENTITY_FEATURE_COUNT + 4;

    /**
     * The number of operations of the '<em>Comment</em>' class. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int COMMENT_OPERATION_COUNT = ENTITY_OPERATION_COUNT + 0;

    /**
     * The meta object id for the '{@link earthnewmedia.model.ContentMetadata
     * <em>Content Metadata</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc
     * -->
     * 
     * @see earthnewmedia.model.ContentMetadata
     * @see earthnewmedia.model.impl.ModelPackageImpl#getContentMetadata()
     * @generated
     */
    int CONTENT_METADATA = 7;

    /**
     * The feature id for the '<em><b>Uri</b></em>' attribute. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONTENT_METADATA__URI = 0;

    /**
     * The feature id for the '<em><b>Mimetype</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONTENT_METADATA__MIMETYPE = 1;

    /**
     * The number of structural features of the '<em>Content Metadata</em>' class.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONTENT_METADATA_FEATURE_COUNT = 2;

    /**
     * The number of operations of the '<em>Content Metadata</em>' class. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int CONTENT_METADATA_OPERATION_COUNT = 0;

    /**
     * The meta object id for the
     * '{@link earthnewmedia.model.impl.ArticleMetadataImpl <em>Article
     * Metadata</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see earthnewmedia.model.impl.ArticleMetadataImpl
     * @see earthnewmedia.model.impl.ModelPackageImpl#getArticleMetadata()
     * @generated
     */
    int ARTICLE_METADATA = 8;

    /**
     * The feature id for the '<em><b>Uri</b></em>' attribute. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ARTICLE_METADATA__URI = CONTENT_METADATA__URI;

    /**
     * The feature id for the '<em><b>Mimetype</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ARTICLE_METADATA__MIMETYPE = CONTENT_METADATA__MIMETYPE;

    /**
     * The feature id for the '<em><b>Image</b></em>' attribute. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ARTICLE_METADATA__IMAGE = CONTENT_METADATA_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>Article Metadata</em>' class.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ARTICLE_METADATA_FEATURE_COUNT = CONTENT_METADATA_FEATURE_COUNT + 1;

    /**
     * The number of operations of the '<em>Article Metadata</em>' class. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int ARTICLE_METADATA_OPERATION_COUNT = CONTENT_METADATA_OPERATION_COUNT + 0;

    /**
     * The meta object id for the '{@link earthnewmedia.model.impl.ImageMetadataImpl
     * <em>Image Metadata</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc
     * -->
     * 
     * @see earthnewmedia.model.impl.ImageMetadataImpl
     * @see earthnewmedia.model.impl.ModelPackageImpl#getImageMetadata()
     * @generated
     */
    int IMAGE_METADATA = 9;

    /**
     * The feature id for the '<em><b>Uri</b></em>' attribute. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int IMAGE_METADATA__URI = CONTENT_METADATA__URI;

    /**
     * The feature id for the '<em><b>Mimetype</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int IMAGE_METADATA__MIMETYPE = CONTENT_METADATA__MIMETYPE;

    /**
     * The feature id for the '<em><b>Width</b></em>' attribute. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int IMAGE_METADATA__WIDTH = CONTENT_METADATA_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Height</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int IMAGE_METADATA__HEIGHT = CONTENT_METADATA_FEATURE_COUNT + 1;

    /**
     * The number of structural features of the '<em>Image Metadata</em>' class.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int IMAGE_METADATA_FEATURE_COUNT = CONTENT_METADATA_FEATURE_COUNT + 2;

    /**
     * The number of operations of the '<em>Image Metadata</em>' class. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int IMAGE_METADATA_OPERATION_COUNT = CONTENT_METADATA_OPERATION_COUNT + 0;

    /**
     * The meta object id for the '{@link earthnewmedia.model.impl.AudioMetadataImpl
     * <em>Audio Metadata</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc
     * -->
     * 
     * @see earthnewmedia.model.impl.AudioMetadataImpl
     * @see earthnewmedia.model.impl.ModelPackageImpl#getAudioMetadata()
     * @generated
     */
    int AUDIO_METADATA = 10;

    /**
     * The feature id for the '<em><b>Uri</b></em>' attribute. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int AUDIO_METADATA__URI = CONTENT_METADATA__URI;

    /**
     * The feature id for the '<em><b>Mimetype</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int AUDIO_METADATA__MIMETYPE = CONTENT_METADATA__MIMETYPE;

    /**
     * The feature id for the '<em><b>Duration</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int AUDIO_METADATA__DURATION = CONTENT_METADATA_FEATURE_COUNT + 0;

    /**
     * The number of structural features of the '<em>Audio Metadata</em>' class.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int AUDIO_METADATA_FEATURE_COUNT = CONTENT_METADATA_FEATURE_COUNT + 1;

    /**
     * The number of operations of the '<em>Audio Metadata</em>' class. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int AUDIO_METADATA_OPERATION_COUNT = CONTENT_METADATA_OPERATION_COUNT + 0;

    /**
     * The meta object id for the '{@link earthnewmedia.model.impl.VideoMetadataImpl
     * <em>Video Metadata</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc
     * -->
     * 
     * @see earthnewmedia.model.impl.VideoMetadataImpl
     * @see earthnewmedia.model.impl.ModelPackageImpl#getVideoMetadata()
     * @generated
     */
    int VIDEO_METADATA = 11;

    /**
     * The feature id for the '<em><b>Uri</b></em>' attribute. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VIDEO_METADATA__URI = CONTENT_METADATA__URI;

    /**
     * The feature id for the '<em><b>Mimetype</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VIDEO_METADATA__MIMETYPE = CONTENT_METADATA__MIMETYPE;

    /**
     * The feature id for the '<em><b>Duration</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VIDEO_METADATA__DURATION = CONTENT_METADATA_FEATURE_COUNT + 0;

    /**
     * The feature id for the '<em><b>Width</b></em>' attribute. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VIDEO_METADATA__WIDTH = CONTENT_METADATA_FEATURE_COUNT + 1;

    /**
     * The feature id for the '<em><b>Height</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VIDEO_METADATA__HEIGHT = CONTENT_METADATA_FEATURE_COUNT + 2;

    /**
     * The number of structural features of the '<em>Video Metadata</em>' class.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VIDEO_METADATA_FEATURE_COUNT = CONTENT_METADATA_FEATURE_COUNT + 3;

    /**
     * The number of operations of the '<em>Video Metadata</em>' class. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     * @ordered
     */
    int VIDEO_METADATA_OPERATION_COUNT = CONTENT_METADATA_OPERATION_COUNT + 0;

    /**
     * The meta object id for the '{@link earthnewmedia.model.AccessLevel <em>Access
     * Level</em>}' enum. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see earthnewmedia.model.AccessLevel
     * @see earthnewmedia.model.impl.ModelPackageImpl#getAccessLevel()
     * @generated
     */
    int ACCESS_LEVEL = 12;

    /**
     * The meta object id for the '{@link earthnewmedia.model.UserRole <em>User
     * Role</em>}' enum. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see earthnewmedia.model.UserRole
     * @see earthnewmedia.model.impl.ModelPackageImpl#getUserRole()
     * @generated
     */
    int USER_ROLE = 13;

    /**
     * The meta object id for the '{@link earthnewmedia.model.HashAlgo <em>Hash
     * Algo</em>}' enum. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see earthnewmedia.model.HashAlgo
     * @see earthnewmedia.model.impl.ModelPackageImpl#getHashAlgo()
     * @generated
     */
    int HASH_ALGO = 14;

    /**
     * The meta object id for the '{@link earthnewmedia.model.LibraryType
     * <em>Library Type</em>}' enum. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @see earthnewmedia.model.LibraryType
     * @see earthnewmedia.model.impl.ModelPackageImpl#getLibraryType()
     * @generated
     */
    int LIBRARY_TYPE = 15;

    /**
     * Returns the meta object for class '{@link earthnewmedia.model.Entity
     * <em>Entity</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Entity</em>'.
     * @see earthnewmedia.model.Entity
     * @generated
     */
    EClass getEntity();

    /**
     * Returns the meta object for the attribute
     * '{@link earthnewmedia.model.Entity#getId <em>Id</em>}'. <!-- begin-user-doc
     * --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Id</em>'.
     * @see earthnewmedia.model.Entity#getId()
     * @see #getEntity()
     * @generated
     */
    EAttribute getEntity_Id();

    /**
     * Returns the meta object for class '{@link earthnewmedia.model.User
     * <em>User</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>User</em>'.
     * @see earthnewmedia.model.User
     * @generated
     */
    EClass getUser();

    /**
     * Returns the meta object for the attribute
     * '{@link earthnewmedia.model.User#getUserName <em>User Name</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>User Name</em>'.
     * @see earthnewmedia.model.User#getUserName()
     * @see #getUser()
     * @generated
     */
    EAttribute getUser_UserName();

    /**
     * Returns the meta object for the containment reference
     * '{@link earthnewmedia.model.User#getCredentials <em>Credentials</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>Credentials</em>'.
     * @see earthnewmedia.model.User#getCredentials()
     * @see #getUser()
     * @generated
     */
    EReference getUser_Credentials();

    /**
     * Returns the meta object for the attribute
     * '{@link earthnewmedia.model.User#getEmail <em>Email</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Email</em>'.
     * @see earthnewmedia.model.User#getEmail()
     * @see #getUser()
     * @generated
     */
    EAttribute getUser_Email();

    /**
     * Returns the meta object for the containment reference list
     * '{@link earthnewmedia.model.User#getAttributes <em>Attributes</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list
     *         '<em>Attributes</em>'.
     * @see earthnewmedia.model.User#getAttributes()
     * @see #getUser()
     * @generated
     */
    EReference getUser_Attributes();

    /**
     * Returns the meta object for the containment reference list
     * '{@link earthnewmedia.model.User#getLibraries <em>Libraries</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list
     *         '<em>Libraries</em>'.
     * @see earthnewmedia.model.User#getLibraries()
     * @see #getUser()
     * @generated
     */
    EReference getUser_Libraries();

    /**
     * Returns the meta object for class '{@link earthnewmedia.model.Credentials
     * <em>Credentials</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Credentials</em>'.
     * @see earthnewmedia.model.Credentials
     * @generated
     */
    EClass getCredentials();

    /**
     * Returns the meta object for the attribute
     * '{@link earthnewmedia.model.Credentials#getAlgo <em>Algo</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Algo</em>'.
     * @see earthnewmedia.model.Credentials#getAlgo()
     * @see #getCredentials()
     * @generated
     */
    EAttribute getCredentials_Algo();

    /**
     * Returns the meta object for the attribute
     * '{@link earthnewmedia.model.Credentials#getValue <em>Value</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Value</em>'.
     * @see earthnewmedia.model.Credentials#getValue()
     * @see #getCredentials()
     * @generated
     */
    EAttribute getCredentials_Value();

    /**
     * Returns the meta object for class '{@link earthnewmedia.model.MemberAttribute
     * <em>Member Attribute</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Member Attribute</em>'.
     * @see earthnewmedia.model.MemberAttribute
     * @generated
     */
    EClass getMemberAttribute();

    /**
     * Returns the meta object for the attribute
     * '{@link earthnewmedia.model.MemberAttribute#getLabel <em>Label</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Label</em>'.
     * @see earthnewmedia.model.MemberAttribute#getLabel()
     * @see #getMemberAttribute()
     * @generated
     */
    EAttribute getMemberAttribute_Label();

    /**
     * Returns the meta object for the attribute
     * '{@link earthnewmedia.model.MemberAttribute#getContent <em>Content</em>}'.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Content</em>'.
     * @see earthnewmedia.model.MemberAttribute#getContent()
     * @see #getMemberAttribute()
     * @generated
     */
    EAttribute getMemberAttribute_Content();

    /**
     * Returns the meta object for class '{@link earthnewmedia.model.Library
     * <em>Library</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Library</em>'.
     * @see earthnewmedia.model.Library
     * @generated
     */
    EClass getLibrary();

    /**
     * Returns the meta object for the attribute
     * '{@link earthnewmedia.model.Library#getSlug <em>Slug</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Slug</em>'.
     * @see earthnewmedia.model.Library#getSlug()
     * @see #getLibrary()
     * @generated
     */
    EAttribute getLibrary_Slug();

    /**
     * Returns the meta object for the attribute
     * '{@link earthnewmedia.model.Library#getName <em>Name</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Name</em>'.
     * @see earthnewmedia.model.Library#getName()
     * @see #getLibrary()
     * @generated
     */
    EAttribute getLibrary_Name();

    /**
     * Returns the meta object for the attribute
     * '{@link earthnewmedia.model.Library#getDescription <em>Description</em>}'.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Description</em>'.
     * @see earthnewmedia.model.Library#getDescription()
     * @see #getLibrary()
     * @generated
     */
    EAttribute getLibrary_Description();

    /**
     * Returns the meta object for the attribute
     * '{@link earthnewmedia.model.Library#getType <em>Type</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Type</em>'.
     * @see earthnewmedia.model.Library#getType()
     * @see #getLibrary()
     * @generated
     */
    EAttribute getLibrary_Type();

    /**
     * Returns the meta object for the attribute
     * '{@link earthnewmedia.model.Library#getCreatedDate <em>Created Date</em>}'.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Created Date</em>'.
     * @see earthnewmedia.model.Library#getCreatedDate()
     * @see #getLibrary()
     * @generated
     */
    EAttribute getLibrary_CreatedDate();

    /**
     * Returns the meta object for the attribute
     * '{@link earthnewmedia.model.Library#getAccess <em>Access</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Access</em>'.
     * @see earthnewmedia.model.Library#getAccess()
     * @see #getLibrary()
     * @generated
     */
    EAttribute getLibrary_Access();

    /**
     * Returns the meta object for the containment reference list
     * '{@link earthnewmedia.model.Library#getItems <em>Items</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list '<em>Items</em>'.
     * @see earthnewmedia.model.Library#getItems()
     * @see #getLibrary()
     * @generated
     */
    EReference getLibrary_Items();

    /**
     * Returns the meta object for class '{@link earthnewmedia.model.LibraryItem
     * <em>Library Item</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Library Item</em>'.
     * @see earthnewmedia.model.LibraryItem
     * @generated
     */
    EClass getLibraryItem();

    /**
     * Returns the meta object for the attribute
     * '{@link earthnewmedia.model.LibraryItem#getTitle <em>Title</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Title</em>'.
     * @see earthnewmedia.model.LibraryItem#getTitle()
     * @see #getLibraryItem()
     * @generated
     */
    EAttribute getLibraryItem_Title();

    /**
     * Returns the meta object for the attribute
     * '{@link earthnewmedia.model.LibraryItem#getDescription
     * <em>Description</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Description</em>'.
     * @see earthnewmedia.model.LibraryItem#getDescription()
     * @see #getLibraryItem()
     * @generated
     */
    EAttribute getLibraryItem_Description();

    /**
     * Returns the meta object for the containment reference list
     * '{@link earthnewmedia.model.LibraryItem#getComments <em>Comments</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list
     *         '<em>Comments</em>'.
     * @see earthnewmedia.model.LibraryItem#getComments()
     * @see #getLibraryItem()
     * @generated
     */
    EReference getLibraryItem_Comments();

    /**
     * Returns the meta object for the containment reference
     * '{@link earthnewmedia.model.LibraryItem#getMetadata <em>Metadata</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference '<em>Metadata</em>'.
     * @see earthnewmedia.model.LibraryItem#getMetadata()
     * @see #getLibraryItem()
     * @generated
     */
    EReference getLibraryItem_Metadata();

    /**
     * Returns the meta object for the attribute
     * '{@link earthnewmedia.model.LibraryItem#getCreatedDate <em>Created
     * Date</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Created Date</em>'.
     * @see earthnewmedia.model.LibraryItem#getCreatedDate()
     * @see #getLibraryItem()
     * @generated
     */
    EAttribute getLibraryItem_CreatedDate();

    /**
     * Returns the meta object for the attribute
     * '{@link earthnewmedia.model.LibraryItem#getSlug <em>Slug</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Slug</em>'.
     * @see earthnewmedia.model.LibraryItem#getSlug()
     * @see #getLibraryItem()
     * @generated
     */
    EAttribute getLibraryItem_Slug();

    /**
     * Returns the meta object for class '{@link earthnewmedia.model.Comment
     * <em>Comment</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Comment</em>'.
     * @see earthnewmedia.model.Comment
     * @generated
     */
    EClass getComment();

    /**
     * Returns the meta object for the containment reference list
     * '{@link earthnewmedia.model.Comment#getReplies <em>Replies</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the containment reference list
     *         '<em>Replies</em>'.
     * @see earthnewmedia.model.Comment#getReplies()
     * @see #getComment()
     * @generated
     */
    EReference getComment_Replies();

    /**
     * Returns the meta object for the reference
     * '{@link earthnewmedia.model.Comment#getOwner <em>Owner</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the reference '<em>Owner</em>'.
     * @see earthnewmedia.model.Comment#getOwner()
     * @see #getComment()
     * @generated
     */
    EReference getComment_Owner();

    /**
     * Returns the meta object for the attribute
     * '{@link earthnewmedia.model.Comment#getContent <em>Content</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Content</em>'.
     * @see earthnewmedia.model.Comment#getContent()
     * @see #getComment()
     * @generated
     */
    EAttribute getComment_Content();

    /**
     * Returns the meta object for the attribute
     * '{@link earthnewmedia.model.Comment#getCreatedDate <em>Created Date</em>}'.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Created Date</em>'.
     * @see earthnewmedia.model.Comment#getCreatedDate()
     * @see #getComment()
     * @generated
     */
    EAttribute getComment_CreatedDate();

    /**
     * Returns the meta object for class '{@link earthnewmedia.model.ContentMetadata
     * <em>Content Metadata</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Content Metadata</em>'.
     * @see earthnewmedia.model.ContentMetadata
     * @generated
     */
    EClass getContentMetadata();

    /**
     * Returns the meta object for the attribute
     * '{@link earthnewmedia.model.ContentMetadata#getUri <em>Uri</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Uri</em>'.
     * @see earthnewmedia.model.ContentMetadata#getUri()
     * @see #getContentMetadata()
     * @generated
     */
    EAttribute getContentMetadata_Uri();

    /**
     * Returns the meta object for the attribute
     * '{@link earthnewmedia.model.ContentMetadata#getMimetype <em>Mimetype</em>}'.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Mimetype</em>'.
     * @see earthnewmedia.model.ContentMetadata#getMimetype()
     * @see #getContentMetadata()
     * @generated
     */
    EAttribute getContentMetadata_Mimetype();

    /**
     * Returns the meta object for class '{@link earthnewmedia.model.ArticleMetadata
     * <em>Article Metadata</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Article Metadata</em>'.
     * @see earthnewmedia.model.ArticleMetadata
     * @generated
     */
    EClass getArticleMetadata();

    /**
     * Returns the meta object for the attribute
     * '{@link earthnewmedia.model.ArticleMetadata#getImage <em>Image</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Image</em>'.
     * @see earthnewmedia.model.ArticleMetadata#getImage()
     * @see #getArticleMetadata()
     * @generated
     */
    EAttribute getArticleMetadata_Image();

    /**
     * Returns the meta object for class '{@link earthnewmedia.model.ImageMetadata
     * <em>Image Metadata</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Image Metadata</em>'.
     * @see earthnewmedia.model.ImageMetadata
     * @generated
     */
    EClass getImageMetadata();

    /**
     * Returns the meta object for the attribute
     * '{@link earthnewmedia.model.ImageMetadata#getWidth <em>Width</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Width</em>'.
     * @see earthnewmedia.model.ImageMetadata#getWidth()
     * @see #getImageMetadata()
     * @generated
     */
    EAttribute getImageMetadata_Width();

    /**
     * Returns the meta object for the attribute
     * '{@link earthnewmedia.model.ImageMetadata#getHeight <em>Height</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Height</em>'.
     * @see earthnewmedia.model.ImageMetadata#getHeight()
     * @see #getImageMetadata()
     * @generated
     */
    EAttribute getImageMetadata_Height();

    /**
     * Returns the meta object for class '{@link earthnewmedia.model.AudioMetadata
     * <em>Audio Metadata</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Audio Metadata</em>'.
     * @see earthnewmedia.model.AudioMetadata
     * @generated
     */
    EClass getAudioMetadata();

    /**
     * Returns the meta object for the attribute
     * '{@link earthnewmedia.model.AudioMetadata#getDuration <em>Duration</em>}'.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Duration</em>'.
     * @see earthnewmedia.model.AudioMetadata#getDuration()
     * @see #getAudioMetadata()
     * @generated
     */
    EAttribute getAudioMetadata_Duration();

    /**
     * Returns the meta object for class '{@link earthnewmedia.model.VideoMetadata
     * <em>Video Metadata</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for class '<em>Video Metadata</em>'.
     * @see earthnewmedia.model.VideoMetadata
     * @generated
     */
    EClass getVideoMetadata();

    /**
     * Returns the meta object for the attribute
     * '{@link earthnewmedia.model.VideoMetadata#getDuration <em>Duration</em>}'.
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Duration</em>'.
     * @see earthnewmedia.model.VideoMetadata#getDuration()
     * @see #getVideoMetadata()
     * @generated
     */
    EAttribute getVideoMetadata_Duration();

    /**
     * Returns the meta object for the attribute
     * '{@link earthnewmedia.model.VideoMetadata#getWidth <em>Width</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Width</em>'.
     * @see earthnewmedia.model.VideoMetadata#getWidth()
     * @see #getVideoMetadata()
     * @generated
     */
    EAttribute getVideoMetadata_Width();

    /**
     * Returns the meta object for the attribute
     * '{@link earthnewmedia.model.VideoMetadata#getHeight <em>Height</em>}'. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for the attribute '<em>Height</em>'.
     * @see earthnewmedia.model.VideoMetadata#getHeight()
     * @see #getVideoMetadata()
     * @generated
     */
    EAttribute getVideoMetadata_Height();

    /**
     * Returns the meta object for enum '{@link earthnewmedia.model.AccessLevel
     * <em>Access Level</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>Access Level</em>'.
     * @see earthnewmedia.model.AccessLevel
     * @generated
     */
    EEnum getAccessLevel();

    /**
     * Returns the meta object for enum '{@link earthnewmedia.model.UserRole
     * <em>User Role</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>User Role</em>'.
     * @see earthnewmedia.model.UserRole
     * @generated
     */
    EEnum getUserRole();

    /**
     * Returns the meta object for enum '{@link earthnewmedia.model.HashAlgo
     * <em>Hash Algo</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>Hash Algo</em>'.
     * @see earthnewmedia.model.HashAlgo
     * @generated
     */
    EEnum getHashAlgo();

    /**
     * Returns the meta object for enum '{@link earthnewmedia.model.LibraryType
     * <em>Library Type</em>}'. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the meta object for enum '<em>Library Type</em>'.
     * @see earthnewmedia.model.LibraryType
     * @generated
     */
    EEnum getLibraryType();

    /**
     * Returns the factory that creates the instances of the model. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the factory that creates the instances of the model.
     * @generated
     */
    ModelFactory getModelFactory();

    /**
     * <!-- begin-user-doc --> Defines literals for the meta objects that represent
     * <ul>
     * <li>each class,</li>
     * <li>each feature of each class,</li>
     * <li>each operation of each class,</li>
     * <li>each enum,</li>
     * <li>and each data type</li>
     * </ul>
     * <!-- end-user-doc -->
     * 
     * @generated
     */
    interface Literals {
        /**
         * The meta object literal for the '{@link earthnewmedia.model.Entity
         * <em>Entity</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see earthnewmedia.model.Entity
         * @see earthnewmedia.model.impl.ModelPackageImpl#getEntity()
         * @generated
         */
        EClass ENTITY = eINSTANCE.getEntity();

        /**
         * The meta object literal for the '<em><b>Id</b></em>' attribute feature. <!--
         * begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute ENTITY__ID = eINSTANCE.getEntity_Id();

        /**
         * The meta object literal for the '{@link earthnewmedia.model.impl.UserImpl
         * <em>User</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see earthnewmedia.model.impl.UserImpl
         * @see earthnewmedia.model.impl.ModelPackageImpl#getUser()
         * @generated
         */
        EClass USER = eINSTANCE.getUser();

        /**
         * The meta object literal for the '<em><b>User Name</b></em>' attribute
         * feature. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute USER__USER_NAME = eINSTANCE.getUser_UserName();

        /**
         * The meta object literal for the '<em><b>Credentials</b></em>' containment
         * reference feature. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference USER__CREDENTIALS = eINSTANCE.getUser_Credentials();

        /**
         * The meta object literal for the '<em><b>Email</b></em>' attribute feature.
         * <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute USER__EMAIL = eINSTANCE.getUser_Email();

        /**
         * The meta object literal for the '<em><b>Attributes</b></em>' containment
         * reference list feature. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference USER__ATTRIBUTES = eINSTANCE.getUser_Attributes();

        /**
         * The meta object literal for the '<em><b>Libraries</b></em>' containment
         * reference list feature. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference USER__LIBRARIES = eINSTANCE.getUser_Libraries();

        /**
         * The meta object literal for the
         * '{@link earthnewmedia.model.impl.CredentialsImpl <em>Credentials</em>}'
         * class. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see earthnewmedia.model.impl.CredentialsImpl
         * @see earthnewmedia.model.impl.ModelPackageImpl#getCredentials()
         * @generated
         */
        EClass CREDENTIALS = eINSTANCE.getCredentials();

        /**
         * The meta object literal for the '<em><b>Algo</b></em>' attribute feature.
         * <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CREDENTIALS__ALGO = eINSTANCE.getCredentials_Algo();

        /**
         * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
         * <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CREDENTIALS__VALUE = eINSTANCE.getCredentials_Value();

        /**
         * The meta object literal for the
         * '{@link earthnewmedia.model.impl.MemberAttributeImpl <em>Member
         * Attribute</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see earthnewmedia.model.impl.MemberAttributeImpl
         * @see earthnewmedia.model.impl.ModelPackageImpl#getMemberAttribute()
         * @generated
         */
        EClass MEMBER_ATTRIBUTE = eINSTANCE.getMemberAttribute();

        /**
         * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
         * <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute MEMBER_ATTRIBUTE__LABEL = eINSTANCE.getMemberAttribute_Label();

        /**
         * The meta object literal for the '<em><b>Content</b></em>' attribute feature.
         * <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute MEMBER_ATTRIBUTE__CONTENT = eINSTANCE.getMemberAttribute_Content();

        /**
         * The meta object literal for the '{@link earthnewmedia.model.impl.LibraryImpl
         * <em>Library</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see earthnewmedia.model.impl.LibraryImpl
         * @see earthnewmedia.model.impl.ModelPackageImpl#getLibrary()
         * @generated
         */
        EClass LIBRARY = eINSTANCE.getLibrary();

        /**
         * The meta object literal for the '<em><b>Slug</b></em>' attribute feature.
         * <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute LIBRARY__SLUG = eINSTANCE.getLibrary_Slug();

        /**
         * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
         * <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute LIBRARY__NAME = eINSTANCE.getLibrary_Name();

        /**
         * The meta object literal for the '<em><b>Description</b></em>' attribute
         * feature. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute LIBRARY__DESCRIPTION = eINSTANCE.getLibrary_Description();

        /**
         * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
         * <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute LIBRARY__TYPE = eINSTANCE.getLibrary_Type();

        /**
         * The meta object literal for the '<em><b>Created Date</b></em>' attribute
         * feature. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute LIBRARY__CREATED_DATE = eINSTANCE.getLibrary_CreatedDate();

        /**
         * The meta object literal for the '<em><b>Access</b></em>' attribute feature.
         * <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute LIBRARY__ACCESS = eINSTANCE.getLibrary_Access();

        /**
         * The meta object literal for the '<em><b>Items</b></em>' containment reference
         * list feature. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference LIBRARY__ITEMS = eINSTANCE.getLibrary_Items();

        /**
         * The meta object literal for the
         * '{@link earthnewmedia.model.impl.LibraryItemImpl <em>Library Item</em>}'
         * class. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see earthnewmedia.model.impl.LibraryItemImpl
         * @see earthnewmedia.model.impl.ModelPackageImpl#getLibraryItem()
         * @generated
         */
        EClass LIBRARY_ITEM = eINSTANCE.getLibraryItem();

        /**
         * The meta object literal for the '<em><b>Title</b></em>' attribute feature.
         * <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute LIBRARY_ITEM__TITLE = eINSTANCE.getLibraryItem_Title();

        /**
         * The meta object literal for the '<em><b>Description</b></em>' attribute
         * feature. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute LIBRARY_ITEM__DESCRIPTION = eINSTANCE.getLibraryItem_Description();

        /**
         * The meta object literal for the '<em><b>Comments</b></em>' containment
         * reference list feature. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference LIBRARY_ITEM__COMMENTS = eINSTANCE.getLibraryItem_Comments();

        /**
         * The meta object literal for the '<em><b>Metadata</b></em>' containment
         * reference feature. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference LIBRARY_ITEM__METADATA = eINSTANCE.getLibraryItem_Metadata();

        /**
         * The meta object literal for the '<em><b>Created Date</b></em>' attribute
         * feature. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute LIBRARY_ITEM__CREATED_DATE = eINSTANCE.getLibraryItem_CreatedDate();

        /**
         * The meta object literal for the '<em><b>Slug</b></em>' attribute feature.
         * <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute LIBRARY_ITEM__SLUG = eINSTANCE.getLibraryItem_Slug();

        /**
         * The meta object literal for the '{@link earthnewmedia.model.impl.CommentImpl
         * <em>Comment</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see earthnewmedia.model.impl.CommentImpl
         * @see earthnewmedia.model.impl.ModelPackageImpl#getComment()
         * @generated
         */
        EClass COMMENT = eINSTANCE.getComment();

        /**
         * The meta object literal for the '<em><b>Replies</b></em>' containment
         * reference list feature. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference COMMENT__REPLIES = eINSTANCE.getComment_Replies();

        /**
         * The meta object literal for the '<em><b>Owner</b></em>' reference feature.
         * <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EReference COMMENT__OWNER = eINSTANCE.getComment_Owner();

        /**
         * The meta object literal for the '<em><b>Content</b></em>' attribute feature.
         * <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute COMMENT__CONTENT = eINSTANCE.getComment_Content();

        /**
         * The meta object literal for the '<em><b>Created Date</b></em>' attribute
         * feature. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute COMMENT__CREATED_DATE = eINSTANCE.getComment_CreatedDate();

        /**
         * The meta object literal for the '{@link earthnewmedia.model.ContentMetadata
         * <em>Content Metadata</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc
         * -->
         * 
         * @see earthnewmedia.model.ContentMetadata
         * @see earthnewmedia.model.impl.ModelPackageImpl#getContentMetadata()
         * @generated
         */
        EClass CONTENT_METADATA = eINSTANCE.getContentMetadata();

        /**
         * The meta object literal for the '<em><b>Uri</b></em>' attribute feature. <!--
         * begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CONTENT_METADATA__URI = eINSTANCE.getContentMetadata_Uri();

        /**
         * The meta object literal for the '<em><b>Mimetype</b></em>' attribute feature.
         * <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute CONTENT_METADATA__MIMETYPE = eINSTANCE.getContentMetadata_Mimetype();

        /**
         * The meta object literal for the
         * '{@link earthnewmedia.model.impl.ArticleMetadataImpl <em>Article
         * Metadata</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see earthnewmedia.model.impl.ArticleMetadataImpl
         * @see earthnewmedia.model.impl.ModelPackageImpl#getArticleMetadata()
         * @generated
         */
        EClass ARTICLE_METADATA = eINSTANCE.getArticleMetadata();

        /**
         * The meta object literal for the '<em><b>Image</b></em>' attribute feature.
         * <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute ARTICLE_METADATA__IMAGE = eINSTANCE.getArticleMetadata_Image();

        /**
         * The meta object literal for the
         * '{@link earthnewmedia.model.impl.ImageMetadataImpl <em>Image Metadata</em>}'
         * class. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see earthnewmedia.model.impl.ImageMetadataImpl
         * @see earthnewmedia.model.impl.ModelPackageImpl#getImageMetadata()
         * @generated
         */
        EClass IMAGE_METADATA = eINSTANCE.getImageMetadata();

        /**
         * The meta object literal for the '<em><b>Width</b></em>' attribute feature.
         * <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute IMAGE_METADATA__WIDTH = eINSTANCE.getImageMetadata_Width();

        /**
         * The meta object literal for the '<em><b>Height</b></em>' attribute feature.
         * <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute IMAGE_METADATA__HEIGHT = eINSTANCE.getImageMetadata_Height();

        /**
         * The meta object literal for the
         * '{@link earthnewmedia.model.impl.AudioMetadataImpl <em>Audio Metadata</em>}'
         * class. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see earthnewmedia.model.impl.AudioMetadataImpl
         * @see earthnewmedia.model.impl.ModelPackageImpl#getAudioMetadata()
         * @generated
         */
        EClass AUDIO_METADATA = eINSTANCE.getAudioMetadata();

        /**
         * The meta object literal for the '<em><b>Duration</b></em>' attribute feature.
         * <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute AUDIO_METADATA__DURATION = eINSTANCE.getAudioMetadata_Duration();

        /**
         * The meta object literal for the
         * '{@link earthnewmedia.model.impl.VideoMetadataImpl <em>Video Metadata</em>}'
         * class. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see earthnewmedia.model.impl.VideoMetadataImpl
         * @see earthnewmedia.model.impl.ModelPackageImpl#getVideoMetadata()
         * @generated
         */
        EClass VIDEO_METADATA = eINSTANCE.getVideoMetadata();

        /**
         * The meta object literal for the '<em><b>Duration</b></em>' attribute feature.
         * <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute VIDEO_METADATA__DURATION = eINSTANCE.getVideoMetadata_Duration();

        /**
         * The meta object literal for the '<em><b>Width</b></em>' attribute feature.
         * <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute VIDEO_METADATA__WIDTH = eINSTANCE.getVideoMetadata_Width();

        /**
         * The meta object literal for the '<em><b>Height</b></em>' attribute feature.
         * <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @generated
         */
        EAttribute VIDEO_METADATA__HEIGHT = eINSTANCE.getVideoMetadata_Height();

        /**
         * The meta object literal for the '{@link earthnewmedia.model.AccessLevel
         * <em>Access Level</em>}' enum. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see earthnewmedia.model.AccessLevel
         * @see earthnewmedia.model.impl.ModelPackageImpl#getAccessLevel()
         * @generated
         */
        EEnum ACCESS_LEVEL = eINSTANCE.getAccessLevel();

        /**
         * The meta object literal for the '{@link earthnewmedia.model.UserRole <em>User
         * Role</em>}' enum. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see earthnewmedia.model.UserRole
         * @see earthnewmedia.model.impl.ModelPackageImpl#getUserRole()
         * @generated
         */
        EEnum USER_ROLE = eINSTANCE.getUserRole();

        /**
         * The meta object literal for the '{@link earthnewmedia.model.HashAlgo <em>Hash
         * Algo</em>}' enum. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see earthnewmedia.model.HashAlgo
         * @see earthnewmedia.model.impl.ModelPackageImpl#getHashAlgo()
         * @generated
         */
        EEnum HASH_ALGO = eINSTANCE.getHashAlgo();

        /**
         * The meta object literal for the '{@link earthnewmedia.model.LibraryType
         * <em>Library Type</em>}' enum. <!-- begin-user-doc --> <!-- end-user-doc -->
         * 
         * @see earthnewmedia.model.LibraryType
         * @see earthnewmedia.model.impl.ModelPackageImpl#getLibraryType()
         * @generated
         */
        EEnum LIBRARY_TYPE = eINSTANCE.getLibraryType();

    }

} // ModelPackage
