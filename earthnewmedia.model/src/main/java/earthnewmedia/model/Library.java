/**
 */
package earthnewmedia.model;

import java.util.Date;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object
 * '<em><b>Library</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 * <li>{@link earthnewmedia.model.Library#getSlug <em>Slug</em>}</li>
 * <li>{@link earthnewmedia.model.Library#getName <em>Name</em>}</li>
 * <li>{@link earthnewmedia.model.Library#getDescription
 * <em>Description</em>}</li>
 * <li>{@link earthnewmedia.model.Library#getType <em>Type</em>}</li>
 * <li>{@link earthnewmedia.model.Library#getCreatedDate <em>Created
 * Date</em>}</li>
 * <li>{@link earthnewmedia.model.Library#getAccess <em>Access</em>}</li>
 * <li>{@link earthnewmedia.model.Library#getItems <em>Items</em>}</li>
 * </ul>
 *
 * @see earthnewmedia.model.ModelPackage#getLibrary()
 * @model
 * @generated
 */
public interface Library extends Entity {
    /**
     * Returns the value of the '<em><b>Slug</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Slug</em>' attribute.
     * @see #setSlug(String)
     * @see earthnewmedia.model.ModelPackage#getLibrary_Slug()
     * @model required="true"
     * @generated
     */
    String getSlug();

    /**
     * Sets the value of the '{@link earthnewmedia.model.Library#getSlug
     * <em>Slug</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Slug</em>' attribute.
     * @see #getSlug()
     * @generated
     */
    void setSlug(String value);

    /**
     * Returns the value of the '<em><b>Name</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Name</em>' attribute.
     * @see #setName(String)
     * @see earthnewmedia.model.ModelPackage#getLibrary_Name()
     * @model required="true"
     * @generated
     */
    String getName();

    /**
     * Sets the value of the '{@link earthnewmedia.model.Library#getName
     * <em>Name</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Name</em>' attribute.
     * @see #getName()
     * @generated
     */
    void setName(String value);

    /**
     * Returns the value of the '<em><b>Description</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Description</em>' attribute.
     * @see #setDescription(String)
     * @see earthnewmedia.model.ModelPackage#getLibrary_Description()
     * @model
     * @generated
     */
    String getDescription();

    /**
     * Sets the value of the '{@link earthnewmedia.model.Library#getDescription
     * <em>Description</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
     * -->
     * 
     * @param value the new value of the '<em>Description</em>' attribute.
     * @see #getDescription()
     * @generated
     */
    void setDescription(String value);

    /**
     * Returns the value of the '<em><b>Type</b></em>' attribute. The literals are
     * from the enumeration {@link earthnewmedia.model.LibraryType}. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Type</em>' attribute.
     * @see earthnewmedia.model.LibraryType
     * @see #setType(LibraryType)
     * @see earthnewmedia.model.ModelPackage#getLibrary_Type()
     * @model required="true"
     * @generated
     */
    LibraryType getType();

    /**
     * Sets the value of the '{@link earthnewmedia.model.Library#getType
     * <em>Type</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Type</em>' attribute.
     * @see earthnewmedia.model.LibraryType
     * @see #getType()
     * @generated
     */
    void setType(LibraryType value);

    /**
     * Returns the value of the '<em><b>Created Date</b></em>' attribute. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Created Date</em>' attribute.
     * @see #setCreatedDate(Date)
     * @see earthnewmedia.model.ModelPackage#getLibrary_CreatedDate()
     * @model required="true"
     * @generated
     */
    Date getCreatedDate();

    /**
     * Sets the value of the '{@link earthnewmedia.model.Library#getCreatedDate
     * <em>Created Date</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
     * -->
     * 
     * @param value the new value of the '<em>Created Date</em>' attribute.
     * @see #getCreatedDate()
     * @generated
     */
    void setCreatedDate(Date value);

    /**
     * Returns the value of the '<em><b>Access</b></em>' attribute. The literals are
     * from the enumeration {@link earthnewmedia.model.AccessLevel}. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Access</em>' attribute.
     * @see earthnewmedia.model.AccessLevel
     * @see #setAccess(AccessLevel)
     * @see earthnewmedia.model.ModelPackage#getLibrary_Access()
     * @model required="true"
     * @generated
     */
    AccessLevel getAccess();

    /**
     * Sets the value of the '{@link earthnewmedia.model.Library#getAccess
     * <em>Access</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param value the new value of the '<em>Access</em>' attribute.
     * @see earthnewmedia.model.AccessLevel
     * @see #getAccess()
     * @generated
     */
    void setAccess(AccessLevel value);

    /**
     * Returns the value of the '<em><b>Items</b></em>' containment reference list.
     * The list contents are of type {@link earthnewmedia.model.LibraryItem}. <!--
     * begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of the '<em>Items</em>' containment reference list.
     * @see earthnewmedia.model.ModelPackage#getLibrary_Items()
     * @model containment="true"
     * @generated
     */
    EList<LibraryItem> getItems();

} // Library
